
import os
import matplotlib.pyplot as plt

def plot_data(epochs, train_error, test_error, fileout, metadata):
    plt.figure()
    plt.plot(epochs, train_error, label='Train', color='b')
    plt.plot(epochs, test_error, label='Test', color='r')
    plt.legend(loc='upper right')
    plt.ylim([0, max(0.1, max(train_error + test_error))])
    plt.xlim([0, 30])
    plt.xlabel('Epochs')
    plt.ylabel('Error')
    plt.title(metadata)
    plt.savefig(fileout)


if __name__ == '__main__':
    dirs = ['SCNN\\', 'CNTK\\']
    for dir in dirs:
        for file in os.listdir(dir):
            if not file.endswith('.csv'):
                continue
            f = open(dir + file, 'rt')
            print(dir + file)
            lines = f.readlines()
            f.close()
            epochs = []
            train_error = []
            test_error = []
            for i in range(1, len(lines)):
                line = lines[i][:-1]
                if len(lines[i]) <= 2:
                    continue
                tokens = line.split(',')
                epochs.append(int(tokens[0]) + 1)
                train_error.append(float(tokens[1]))
                test_error.append(float(tokens[2]))
            if len(epochs) >= 1:
                plot_data(epochs, train_error, test_error, dir + file + '.png', lines[0])





