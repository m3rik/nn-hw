#include "Trainer.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace std::chrono;

Trainer::Trainer(NeuralNet *net, Optimizer optimizer, double learning_rate, double momentum, int batch_size, int iterations)
{
	this->net = net;
	this->learning_rate = learning_rate;
	this->momentum = momentum;
	this->batch_size = batch_size;
	this->iterations = iterations;
	this->optimizer = optimizer;
}


Trainer::~Trainer()
{
}

void Trainer::train(vector<Tensor> &inputs, vector<Tensor> &outputs, bool debug, bool normalize_error)
{
	net->setTrainMode(true);
	net->setOptimizer(optimizer);
	net->setNormalizeError(normalize_error);
	vector<Layer*> *layers = net->getLayers();
	for (int i = 0; i < layers->size(); i++)
	{
		(*layers)[i]->learning_rate = learning_rate / batch_size;
		(*layers)[i]->momentum = momentum;
	}
	
	for (int iteration = 0; iteration < iterations; iteration++)
	{
		high_resolution_clock::time_point t1, t2;
		randomize(inputs, outputs);
		if (debug)
		{
			 t1 = high_resolution_clock::now();
		}
		trainOne(inputs, outputs);
		if (debug) {
			t2 = high_resolution_clock::now();
			auto duration = duration_cast<microseconds>(t2 - t1).count();
			cout << "<< " << (double)duration / 1000000 << " >> " << endl;
		}
	}
	net->setTrainMode(false);
}

void Trainer::randomize(vector<Tensor> &inputs, vector<Tensor> &outputs)
{
	for (int i = 0; i < inputs.size(); i++)
	{
		int a = rand() % inputs.size();
		int b = rand() % inputs.size();
		Tensor aux_in, aux_out;
		aux_in = inputs[a];
		inputs[a] = inputs[b];
		inputs[b] = aux_in;

		aux_out = outputs[a];
		outputs[a] = outputs[b];
		outputs[b] = aux_out;
	}
}

void Trainer::trainOne(vector<Tensor> &inputs, vector<Tensor> &outputs)
{
	
	int batch_count = inputs.size() / batch_size;
	for (int batch_id = 0; batch_id < batch_count; batch_id++)
	{
		for (int item_id = 0; item_id < batch_size; item_id++)
		{
			//inputs[batch_id * batch_size + item_id].print();
			//outputs[batch_id * batch_size + item_id].print();

			net->forward(inputs[batch_id * batch_size + item_id]);
			net->backPropagation(outputs[batch_id * batch_size + item_id]);
			net->updateWeights(inputs[batch_id * batch_size + item_id]);
		}
		net->applyUpdates();		
	}
}

double Trainer::test(vector<Tensor> &inputs, vector<Tensor> &targets, bool(*verify)(Tensor& output, Tensor& target))
{
	double correct = 0;
	for (int i = 0; i < inputs.size(); i++)
	{
		Tensor output = net->forward(inputs[i]);
		if (verify(output, targets[i]))
		{
			correct++;
		}
	}

	return (1.0 - correct / inputs.size());
}


double Trainer::test(vector<Tensor> &inputs, vector<Tensor> &targets, double(*verify)(Tensor& output, Tensor& target))
{
	double error = 0;
	for (int i = 0; i < inputs.size(); i++)
	{
		Tensor output = net->forward(inputs[i]);
		error += verify(output, targets[i]);
	}

	return error;
}
