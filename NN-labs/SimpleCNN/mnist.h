#ifndef MNIST_H_
#define MNIST_H_

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void read_image_file(char *path, double ***inputs, int *len, int max);
void read_label_file(char *path, double ***outputs, int *len, int max);

#endif