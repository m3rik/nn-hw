#pragma once
#include "Layer.h"
class SpacialMaxPooling :
	public Layer
{
	int input;
	int field;
	int width, height;
	
	Tensor outputs;
	Tensor doutputs;
	Tensor errors;
	Tensor switches;

public:
	SpacialMaxPooling(int input, int field, int height, int width);
	~SpacialMaxPooling();

	virtual Tensor forward(Tensor input);

	virtual bool backPropagation(Tensor target, Layer *next);
	virtual bool backPropagation(Layer *last, Layer *next);
	virtual bool backPropagation(Tensor errors, Tensor doutputs);
	
	virtual Tensor updateWeights(Tensor input);

	virtual void saveTo(FILE *f);
	static Layer* loadFrom(FILE *f);
};

