#include "Tensor.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cassert>
#include <random>


Tensor::Tensor(int d3, int d2, int d1)
{
	this->d3 = d3;
	this->d2 = d2;
	this->d1 = d1;
	this->l_sz = d2 * d3;
	this->sz = d1 * d2 * d3;
	values = new double[sz];
}

Tensor::Tensor(double *values, int d3, int d2, int d1)
{
	this->d3 = d3;
	this->d2 = d2;
	this->d1 = d1;
	this->sz = d1 * d2 * d3;
	this->values = values;
}

Tensor::Tensor()
{
	values = NULL;
}

Tensor::~Tensor()
{
}


double& Tensor::operator[] (const int nIndex)
{
	assert(nIndex < sz);
	return values[nIndex];
}

void Tensor::zeros(void)
{
	memset(values, 0, sizeof(double) * sz);
}

void Tensor::shuffle(void)
{
	for (int i = 0; i < sz; i++)
	{
		int i1 = rand() % sz;
		int i2 = rand() % sz;
		double aux = values[i1];
		values[i1] = values[i2];
		values[i2] = aux;
	}
}

void Tensor::randInit(double low, double high)
{
	for (int i = 0; i < sz; i++)
	{
		values[i] = ((double)rand() / RAND_MAX) * (high - low) + low;
	}
}

void Tensor::constInit(double value)
{
	for (int i = 0; i < sz; i++)
	{
		values[i] = value;
	}
}

void Tensor::uniformInit(double low, double high)
{
	double step = (high - low) / sz;
	for (int i = 0; i < sz; i++)
	{
		values[i] = i * step + low;
	}

	shuffle();
}

void Tensor::gaussianInit(double mean, double stddev)
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::normal_distribution<double> distribution(mean, stddev);
	for (int i = 0; i < sz; i++)
	{
		values[i] = distribution(mt);
	}
}

int Tensor::indexOfMax()
{
	int index = 0;
	double max = values[0];
	for (int i = 0; i < sz; i++)
	{
		if (values[i] > max)
		{
			index = i;
			max = values[i];
		}
	}

	
	return index;
}

void Tensor::print()
{
	for (int i = 0; i < d1; i++)
	{
		for (int j = 0; j < d2; j++)
		{
			for (int k = 0; k < d3; k++)
			{
				//printf("%d", values[i * l_sz + j * d3 + k] > 0.5 ? 1 : 0);
				printf("%f ", values[i * l_sz + j * d3 + k]);
			}
			printf("\n");
		}
		printf("\n\n");
	}
}

void Tensor::free()
{
	if (values != NULL)
	{
		delete values;
	}
}

void Tensor::saveTo(FILE *f)
{
	fprintf(f, "%d %d %d\n", d3, d2, d1);
	
	for (int i = 0; i < sz; i++)
	{
		fprintf(f, "%le ", values[i]);
	}

	fprintf(f, "\n");
}

Tensor Tensor::loadFrom(FILE *f)
{
	int d1, d2, d3;
	fscanf(f, "%d %d %d", &d3, &d2, &d1);
	Tensor tensor(d3, d2, d1);
	for (int i = 0; i < tensor.sz; i++)
	{
		fscanf(f, "%le", &tensor[i]);
	}

	return tensor;
}

void Tensor::copyTo(Tensor &t)
{
	assert(t.sz == sz);
	memcpy(t.values, values, sizeof(double) * sz);
}