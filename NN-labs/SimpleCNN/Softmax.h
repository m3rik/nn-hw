#ifndef _SOFTMAX_H_
#define _SOFTMAX_H_

#include "Tensor.h"
#include "Layer.h"

class Softmax :
	public Layer
{
public:
	Softmax();
	~Softmax();
	virtual Tensor forward(Tensor input);
	virtual void derivative(Tensor output, Tensor doutputs);
};

#endif