#ifndef _LAYER_H_
#define _LAYER_H_

#include "Tensor.h"
#include "WeightInit.h"
#include "Parallel.h"
#include "Optimizer.h"
#include "MathUtils.h"

#include <cstdio>
#include <vector>

enum LayerType
{
	LinearType = 0,
	ConvolutionalType = 1,
	MaxPoolingType = 2,
	SigmoidType = 3,
	TanhType = 4,
	LogSoftmaxType = 5,
	SoftmaxType = 6,
	FastSigmoidType = 7,
	ReLUType = 8,
	DropoutType = 9,
};

char *layerTypeToString(LayerType type);

class Layer
{
public:
	bool train_mode;
	double learning_rate, momentum;
	LayerType type;
	Optimizer optimizer;
	bool normalize_error;
	float weightDecay;

	Layer();
	virtual ~Layer();
	virtual Tensor forward(Tensor input);
	virtual void derivative(Tensor output, Tensor doutputs);
	
	virtual bool backPropagation(Tensor target, Layer *next);
	virtual bool backPropagation(Layer *last, Layer *next);
	virtual bool backPropagation(Tensor errors, Tensor doutputs);

	virtual Tensor updateWeights(Tensor input);
	virtual void applyWeights();
	
	virtual void weightsInit(WeightInit *initializer);
	virtual void zeroUpdates();

	virtual void saveTo(FILE *f);

	virtual void setOptimizer(Optimizer optimizer);
	virtual void setWeightDecay(float weightDecay);
};

#endif
