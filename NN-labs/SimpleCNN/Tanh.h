#pragma once

#include "Layer.h"

class Tanh :
	public Layer
{
public:
	Tanh();
	~Tanh();
	virtual Tensor forward(Tensor input);
	virtual void derivative(Tensor output, Tensor doutputs);
};

