#ifndef _TENSOR_H_
#define _TENSOR_H_

#include <cstdio>

class Tensor
{
	double *values;

public:
	int d1, d2, d3, sz, l_sz;

	Tensor(int d3, int d2 = 1, int d1 = 1);
	Tensor(double *values, int d3, int d2 = 1, int d1 = 1);
	Tensor();
	~Tensor();

	void randInit(double low, double high);
	void uniformInit(double low, double high);
	void constInit(double value);
	void gaussianInit(double mean, double stddev);
	void zeros();
	void shuffle();

	int indexOfMax();

	double& operator[] (const int nIndex);

	void free();
	void print();
	void copyTo(Tensor &t);

	void saveTo(FILE *f);
	static Tensor loadFrom(FILE *f);
};

#endif
