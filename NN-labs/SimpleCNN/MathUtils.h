#ifndef _MATH_UTILS_H_
#define _MATH_UTILS_H_

#include <math.h>


template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

double ExpMinusApprox(double x);

#endif