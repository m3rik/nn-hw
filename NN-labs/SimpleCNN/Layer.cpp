#include "Layer.h"

char *layerTypeToString(LayerType type) {
	char *names[] = { "Linear", "Conv", "MaxPool","Sigmoid","Tanh", "LogSoftmax", "Softmax", "FastSigm", "ReLU", "Dropout" };
	return names[type];
}

Layer::Layer()
{
	learning_rate = 0.1;
	momentum = 0.1;
	train_mode = false;
	weightDecay = 0.0005;
}


Layer::~Layer()
{
}

Tensor Layer::forward(Tensor input)
{
	return input;
}

void Layer::weightsInit(WeightInit *initializer)
{

}

bool Layer::backPropagation(Tensor target, Layer *next)
{
	return false;
}

bool Layer::backPropagation(Layer *last, Layer *next)
{
	return false;
}

bool Layer::backPropagation(Tensor errors, Tensor doutputs)
{
	return false;
}

void Layer::derivative(Tensor output, Tensor doutputs)
{
	for (int i = 0; i < doutputs.sz; i++)
	{
		doutputs[i] = 1;
	}
}

Tensor Layer::updateWeights(Tensor input)
{
	return input;
}

void Layer::setOptimizer(Optimizer optimizer)
{
	this->optimizer = optimizer;
}

void Layer::setWeightDecay(float weightDecay)
{
	this->weightDecay = weightDecay;
}

void Layer::applyWeights()
{

}

void Layer::zeroUpdates()
{

}

void Layer::saveTo(FILE *f)
{

}

