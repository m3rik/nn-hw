#pragma once
#include <cassert>

void cifar_load_bin(vector<Tensor> &inputs, vector<Tensor> &outputs, char *path);

void cifar_load_train(vector<Tensor> &inputs, vector<Tensor> &outputs, int n_batches)
{
	char buffer[128];
	for (int i = 1; i <= n_batches; i++)
	{
		sprintf_s(buffer, "../cifar_data/data_batch_%d.bin", i);
		cifar_load_bin(inputs, outputs, buffer);
	}
}

void cifar_load_test(vector<Tensor> &inputs, vector<Tensor> &outputs)
{
	cifar_load_bin(inputs, outputs, "../cifar_data/test_batch.bin");
}

void cifar_load_bin(vector<Tensor> &inputs, vector<Tensor> &outputs, char *path)
{
	FILE *f = fopen(path, "rb");
	assert(f != NULL);
	for (int i = 0; i < 10000; i++)
	{
		Tensor input(32, 32, 3);
		Tensor output(10);
		output.zeros();

		char value;
		fread(&value, sizeof(char), 1, f);
		output[value] = 1;

		for (int c = 0; c < 3; c++)
		{
			for (int y = 0; y < input.d2; y++)
			{
				for (int x = 0; x < input.d3; x++)
				{
					fread(&value, sizeof(char), 1, f);
					input[c * input.l_sz + y * input.d3 + x] = value / 255.0;
				}
			}
		}
		inputs.push_back(input);
		outputs.push_back(output);
	}

	fclose(f);
}