#include <iostream>
#include <chrono>
#include <opencv2\opencv.hpp>

#include "SimpleCNN.h"

#include "HighguiUtils.h"
#include "mnist.h"
#include "cifar10.h"
#include "mat_convertor.h"

using namespace std;
using namespace std::chrono;
using namespace cv;


void mnist_load_train(vector<Tensor> &inputs, vector<Tensor> &outputs, int count)
{
	static int total_count = 0;
	int len;

	double **ins, **outs;
	read_image_file("../mnist_data/images", &ins, &len, total_count + count);
	read_label_file("../mnist_data/labels", &outs, &len, total_count + count);

	inputs.clear();
	outputs.clear();


	for (int i = total_count; i < total_count + count; i++)
	{
		inputs.push_back(Tensor(ins[i], 28, 28, 1));
		outputs.push_back(Tensor(outs[i], 10));
	}

	total_count += count;
}

void mnist_load_test(vector<Tensor> &inputs, vector<Tensor> &outputs, int count)
{
	static int total_count = 0;
	int len;

	double **ins, **outs;
	read_image_file("../mnist_data/t_images", &ins, &len, total_count + count);
	read_label_file("../mnist_data/t_labels", &outs, &len, total_count + count);

	inputs.clear();
	outputs.clear();


	for (int i = total_count; i < total_count + count; i++)
	{
		inputs.push_back(Tensor(ins[i], 28, 28, 1));
		outputs.push_back(Tensor(outs[i], 10));
	}

	total_count += count;
}


bool test_criteria(Tensor &target, Tensor &output)
{
	return target.indexOfMax() == output.indexOfMax();
}

double test_criteria_euclidean(Tensor &target, Tensor &output)
{
	double error = 0;
	for (int i = 0; i < target.sz; i++)
	{
		error += abs(target[i] - output[i]);
	}
	return error;
}

void classes_statistics(vector<Tensor> &outputs)
{
	int classes[10] = { 0 };
	for (unsigned int i = 0; i < outputs.size(); i++)
	{
		classes[outputs[i].indexOfMax()]++;
	}

	for (int i = 0; i < 10; i++)
	{
		cout << classes[i] << " ";
	}
	cout << endl;
}


void train_cifar10()
{
	NeuralNet net;
	net.addLayer(new SpacialConvolution(3, 32, 3, 32, 32, 1, 1));
	net.addLayer(new ReLU());
	net.addLayer(new SpacialConvolution(32, 32, 3, 30, 30, 1, 6));
	net.addLayer(new ReLU());

	net.addLayer(new SpacialConvolution(32, 32, 3, 18, 18, 1, 4));
	net.addLayer(new ReLU());

	net.addLayer(new SpacialConvolution(32, 32, 3, 10, 10, 1, 2));
	net.addLayer(new ReLU());

	net.addLayer(new SpacialConvolution(32, 32, 3, 6, 6, 1, 1));
	net.addLayer(new ReLU());

	net.addLayer(new Linear(32 * 4 * 4, 64));
	net.addLayer(new ReLU());
	net.addLayer(new Linear(64, 10));
	net.addLayer(new Softmax());

	vector<Tensor> inputs, outputs;
	vector<Tensor> tinputs, toutputs;

	float lr = 0.01;
	float momentum = 0.9;
	int batch_size = 16;
	RmspropData data;
	Trainer trainer(&net, Optimizer(OptimizerStrategy::DEFAULT, &data), lr, momentum, batch_size, 1);

	cifar_load_train(inputs, outputs, 5);
	cifar_load_test(tinputs, toutputs);

	/*for (int i = 0; i < inputs.size(); i++)
	{
		Mat m = tensor2mat(inputs[i]);
		imshow("mat", m);
		waitKey(1000);
	}*/

	classes_statistics(outputs);
	classes_statistics(toutputs);


	for (int epoch = 0; epoch < 30; epoch++)
	{
		trainer.train(inputs, outputs, true);
		if ((epoch + 1) % 1 == 0)
		{
			cout << "Iteration: " << epoch + 1 << endl;
			double train_error = trainer.test(inputs, outputs, test_criteria);
			cout << "Train set error: " << train_error << endl;

			double test_error = trainer.test(tinputs, toutputs, test_criteria);
			cout << "Test set error: " << test_error << endl;

		}

		/* Thanks to http://cs231n.github.io/neural-networks-3/#anneal */
		//if ((epoch + 1) % 5 == 0)
		//	trainer.learning_rate /= 2;
	}

	double correct = trainer.test(tinputs, toutputs, test_criteria);
	cout << "Final test set error: " << correct << endl;
}

void train_lenet()
{
	NeuralNet net;
	net.addLayer(new SpacialConvolution(1, 16, 3, 28, 28, 1, 2));
	net.addLayer(new ReLU());
	net.addLayer(new SpacialMaxPooling(16, 2, 24, 24));

	net.addLayer(new SpacialConvolution(16, 32, 3, 12, 12, 1, 2));
	net.addLayer(new ReLU());
	net.addLayer(new SpacialMaxPooling(32, 2, 8, 8));

	net.addLayer(new Linear(32 * 4 * 4, 256));
	net.addLayer(new ReLU());
	net.addLayer(new Dropout(0.1));
	net.addLayer(new Linear(256, 10));
	net.addLayer(new Softmax());
	
	vector<Tensor> inputs, outputs;
	vector<Tensor> tinputs, toutputs;

	float lr = 0.1;
	float momentum = 0.9;
	int batch_size = 64;
	AdadeltaData data;
	Trainer trainer(&net, Optimizer(OptimizerStrategy::DEFAULT, &data), lr, momentum, batch_size, 1);
	FILE *f = fopen("../../Results/SCNN/dummy.csv", "wt");
	fprintf(f, "lr: %f mom: %f batch_size: %d\n", lr, momentum, batch_size);

	mnist_load_train(inputs, outputs, 60000);
	mnist_load_test(tinputs, toutputs, 10000);

	classes_statistics(outputs);
	classes_statistics(toutputs);


	for (int epoch = 0; epoch < 30; epoch++)
	{
		trainer.train(inputs, outputs, true);
		if ((epoch + 1) % 1 == 0)
		{
			cout << "Iteration: " << epoch + 1 << endl;
			double train_error = trainer.test(inputs, outputs, test_criteria);
			cout << "Train set error: " << train_error << endl;

			double test_error = trainer.test(tinputs, toutputs, test_criteria);
			cout << "Test set error: " << test_error << endl;

			fprintf(f, "%d,%lf,%lf\n", epoch, train_error, test_error);
			fflush(f);

			char buffer[1024];
			sprintf(buffer, "../../Networks/mnist_lenet_%d.net", epoch);
			net.saveTo(buffer);
		}

		/* Thanks to http://cs231n.github.io/neural-networks-3/#anneal */
		//if ((epoch + 1) % 5 == 0)
		//	trainer.learning_rate /= 2;
	}

	double correct = trainer.test(tinputs, toutputs, test_criteria);
	cout << "Final test set error: " << correct << endl;
	fclose(f);
}

void test_two_layers_quantized()
{
	clock_t time;
	double test_error = 0;
	NeuralNet net;
	net.loadFrom("../../Networks/mnist_two_layers_29.net");
	net.setTrainMode(false);
	net.printNetwork();

	vector<Tensor> tinputs, toutputs;
	mnist_load_test(tinputs, toutputs, 10000);
	
	float lr = 0.01;
	float momentum = 0.9;
	int batch_size = 128;
	AdagradData data;
	Trainer trainer(&net, Optimizer(OptimizerStrategy::DEFAULT, &data), lr, momentum, batch_size, 1);

	time = clock();
	test_error = trainer.test(tinputs, toutputs, test_criteria);
	printf("Test time: %lf\n", (double)(clock() - time) / CLOCKS_PER_SEC);
	cout << "Original Neural Network: Test set error: " << test_error << endl;

	auto layers = net.getLayers();
	Linear* l1 = (Linear*)layers->at(0);
	l1->quantization(4, 64);

	time = clock();
	test_error = trainer.test(tinputs, toutputs, test_criteria);
	printf("Test time: %lf\n", (double)(clock() - time) / CLOCKS_PER_SEC);
	cout << "Quantized Neural Network: Test set error: " << test_error << endl;
	
}

void train_two_layers()
{
	
	NeuralNet net;
	net.addLayer(new Linear(28 * 28, 1024));
	net.addLayer(new ReLU());
	net.addLayer(new Dropout(0.5));
	net.addLayer(new Linear(1024, 10));
	net.addLayer(new Softmax());


	vector<Tensor> inputs, outputs;
	vector<Tensor> tinputs, toutputs;
	
	float lr = 0.1;
	float momentum = 0.9;
	int batch_size = 64;
	AdagradData data;
	Trainer trainer(&net, Optimizer(OptimizerStrategy::DEFAULT, &data), lr, momentum, batch_size, 1);
	FILE *f = fopen("../../Results/SCNN/test.csv", "wt");
	fprintf(f, "lr: %f mom: %f batch_size: %d\n", lr, momentum, batch_size);

	mnist_load_train(inputs, outputs, 60000);
	mnist_load_test(tinputs, toutputs, 10000);

	classes_statistics(outputs);
	classes_statistics(toutputs);


	for (int epoch = 0; epoch < 30; epoch++)
	{
		trainer.train(inputs, outputs, true);
		if ((epoch + 1) % 1 == 0)
		{
			cout << "Iteration: " << epoch + 1 << endl;
			double train_error = trainer.test(inputs, outputs, test_criteria);
			cout << "Train set error: " << train_error << endl;

			double test_error = trainer.test(tinputs, toutputs, test_criteria);
			cout << "Test set error: " << test_error << endl;

			fprintf(f, "%d,%lf,%lf\n", epoch, train_error, test_error);
			fflush(f);
			char buffer[1024];
			sprintf(buffer, "../../Networks/mnist_two_layers_%d.net", epoch);
			net.saveTo(buffer);

		}

		/* Thanks to http://cs231n.github.io/neural-networks-3/#anneal */
		//if ((epoch + 1) % 5 == 0)
		//	trainer.learning_rate /= 2;
	}

	double correct = trainer.test(tinputs, toutputs, test_criteria);
	cout << "Final test set error: " << correct << endl;
	net.saveTo("../../Networks/mnist_two_layers.net");
	fclose(f);
}

void train_one_layer()
{
	NeuralNet net;
	net.addLayer(new Linear(28 * 28, 10));
	net.addLayer(new Softmax());


	vector<Tensor> inputs, outputs;
	vector<Tensor> tinputs, toutputs;
	Trainer trainer(&net, Optimizer(DEFAULT, NULL), 0.01, 0.9, 32, 1);

	mnist_load_train(inputs, outputs, 60000);
	mnist_load_test(tinputs, toutputs, 10000);

	classes_statistics(outputs);
	classes_statistics(toutputs);


	for (int epoch = 0; epoch < 30; epoch++)
	{
		trainer.train(inputs, outputs);
		if ((epoch + 1) % 1 == 0)
		{
			cout << "Iteration: " << epoch + 1 << endl;
			double correct2 = trainer.test(tinputs, toutputs, test_criteria);
			cout << "Test set accuracy: " << correct2 << endl;
		}
	}
	
	double correct = trainer.test(tinputs, toutputs, test_criteria);
	cout << "Final test set accuracy: " << correct << endl;
}

void load_one_shot_learning(vector<Mat> &inputs, vector<Mat> &outputs, Mat &m, int target_radius, float &rszFactor)
{
	Rect r = BoundingBoxHelper::getROI(m);
	Mat mask = Mat::zeros(m.size(), CV_8UC1);
	int radius = min<int>(r.width / 2, r.height / 2);
	circle(mask, Point(r.x + r.width / 2, r.y + r.height / 2), radius, Scalar(255), -1);

	rszFactor = target_radius * 1.0f / radius;
	
	for (int angle = 0; angle < 360; angle += 5)
	{
		Mat input = getRotatedTemplate(m, angle, INTER_LANCZOS4);
		Mat target = getRotatedTemplate(mask, angle, INTER_NEAREST);
		vector<vector<Point>> contours;
		findContours(target.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
		Rect roi = boundingRect(contours[0]);
		Rect bigRoi(roi.x - roi.width, roi.y - roi.height, roi.width * 3, roi.height * 3);
		
		input = input(bigRoi).clone();
		target = target(bigRoi).clone();
		resize(input, input, Size(0, 0), rszFactor, rszFactor);
		resize(target, target, Size(0, 0), rszFactor, rszFactor);
		inputs.push_back(input);
		outputs.push_back(target);
	}

}

void templatesToTensors(vector<Mat> &inputs, vector<Mat> &outputs, vector<Tensor> &tinputs, vector<Tensor> &toutputs, int insz, int outsz, int channels)
{
	int diff = (insz - outsz) / 2;
	for (int pair = 0; pair < inputs.size(); pair++)
	{
		Mat &input = inputs[pair];
		Mat &output = outputs[pair];
		for (int y = 0; y < input.rows - insz; y += outsz)
		{
			for (int x = 0; x < input.cols - insz; x += outsz)
			{
				tinputs.push_back(mat2tensor(input(Rect(x, y, insz, insz))));
				toutputs.push_back(mat2tensor(output(Rect(x + diff, y + diff, outsz, outsz))));
			}
		}
	}
}


void test_oneshot(Mat &m, NeuralNet &net, int insz, int outsz)
{
	Mat result = Mat::zeros(m.size(), CV_8UC1);
	int diff = (insz - outsz) / 2;
	for (int y = 0; y < m.rows - insz; y += outsz)
	{
		for (int x = 0; x < m.cols - insz; x += outsz)
		{
			Tensor input = mat2tensor(m(Rect(x, y, insz, insz)));
			Tensor output = net.forward(input);

			Mat moutput = tensor2mat(output);
			moutput.copyTo(result(Rect(x + diff, y + diff, outsz, outsz)));
			input.free();
		}
	}
	imshow("testing",result);
	waitKey(500);
}

void train_oneshot()
{
	char *path = "C:\\Users\\Paul\\Desktop\\similarobjectcounting\\Challenge\\scb\\16.JPG";
	Mat m = imread(path);
	float rszFactor = 1920.0f / max<int>(m.cols, m.rows);
	float rszFactor2;
	resize(m, m, Size(0, 0), rszFactor, rszFactor);
	vector<Mat> inputs, outputs;

	int insz = 30;
	int outsz = 6;

	load_one_shot_learning(inputs, outputs, m, outsz * 2, rszFactor2);
	Mat testM;
	resize(m, testM, Size(0, 0), rszFactor2, rszFactor2);
	
	
	NeuralNet net;
	//input: 32 * 32
	net.addLayer(new SpacialConvolution(3, 8, 3, insz, insz, 1, 6));
	net.addLayer(new ReLU());
	net.addLayer(new SpacialConvolution(8, 16, 3, 18, 18, 1, 4));
	net.addLayer(new ReLU());
	net.addLayer(new SpacialConvolution(16, 1, 3, 10, 10, 1, 2));
	net.addLayer(new Sigmoid()); 
	//output: 8 * 8

	vector<Tensor> tinputs, toutputs;
	templatesToTensors(inputs, outputs, tinputs, toutputs, insz, outsz, 3);

	float lr = 0.01;
	float momentum = 0.0;
	int batch_size = 1;
	AdadeltaData data;
	Trainer trainer(&net, Optimizer(OptimizerStrategy::ADADELTA, &data), lr, momentum, batch_size, 1);
	
	for (int epoch = 0; epoch < 30; epoch++)
	{
		trainer.train(tinputs, toutputs, true, true);
		if ((epoch + 1) % 1 == 0)
		{
			cout << "Iteration: " << epoch + 1 << endl;
			double train_error = trainer.test(tinputs, toutputs, test_criteria_euclidean);
			cout << "Train set error: " << train_error << endl;
			test_oneshot(testM, net, insz, outsz);
		}

		/* Thanks to http://cs231n.github.io/neural-networks-3/#anneal */
		//if ((epoch + 1) % 5 == 0)
		//	trainer.learning_rate /= 2;
	}
	for (int i = 0; i < tinputs.size(); i++)
	{
		tinputs[i].free();
		toutputs[i].free();
	}

}

int main()
{
	//train_one_layer();
	//train_two_layers();
	//train_lenet();
	//train_cifar10();
	//train_oneshot();
	//train_two_layers_quantized();
	test_two_layers_quantized();

	

	getchar();
	return 0;
}