#pragma once
#include "Layer.h"
class Sigmoid :
	public Layer
{
public:
	Sigmoid();
	~Sigmoid();
	virtual Tensor forward(Tensor input);
	virtual void derivative(Tensor output, Tensor doutputs);
};

