#include "NeuralNet.h"

#include "Linear.h"
#include "SpacialConvolution.h"
#include "SpacialMaxPooling.h"
#include "Dropout.h"

#include "Sigmoid.h"
#include "Tanh.h"
#include "FastSigmoid.h"
#include "Softmax.h"
#include "LogSoftmax.h"
#include "ReLU.h"

#include <cstdio>
#include <cassert>

NeuralNet::NeuralNet()
{
}


NeuralNet::~NeuralNet()
{
	for (int i = 0; i < layers.size(); i++)
	{
		delete layers[i];
	}
}

void NeuralNet::addLayer(Layer *layer)
{
	layers.push_back(layer);
}

Tensor NeuralNet::forward(Tensor input)
{
	Tensor temp;
	for (int i = 0; i < layers.size(); i++)
	{
		temp = layers[i]->forward(input);
		input = temp;
	}

	return input;
}

vector<Layer*> *NeuralNet::getLayers()
{
	return &layers;
}

void NeuralNet::weightsInit(WeightInit *initializer)
{
	for (int i = 0; i < layers.size(); i++)
	{
		layers[i]->weightsInit(initializer);
	}

	delete initializer;
}

void NeuralNet::backPropagation(Tensor target)
{
	Layer *next = NULL;
	bool result;
	for (int i = layers.size() - 1; i >= 0; i--)
	{
		if (next == NULL)
		{
			result = layers[i]->backPropagation(target, i + 1 < layers.size() ? layers[i + 1] : NULL);
			if (result) {
				next = layers[i];
			}
		}
		else
		{
			result = layers[i]->backPropagation(next, layers[i + 1]);
			if (result) {
				next = layers[i];
			}
		}
	}
}

void NeuralNet::updateWeights(Tensor input)
{
	for (int i = 0; i < layers.size(); i++)
	{
		input = layers[i]->updateWeights(input);
	}
}


void NeuralNet::applyUpdates()
{
	for (int i = 0; i < layers.size(); i++)
	{
		layers[i]->applyWeights();
	}
}

void NeuralNet::zeroUpdates()
{
	for (int i = 0; i < layers.size(); i++)
	{
		layers[i]->zeroUpdates();
	}
}

void NeuralNet::saveTo(char *path)
{
	FILE *f = fopen(path, "wt");
	fprintf(f, "%d\n", layers.size());
	for (int i = 0; i < layers.size(); i++)
	{
		fprintf(f, "%d\n", layers[i]->type);
		layers[i]->saveTo(f);
	}

	fclose(f);
}

void NeuralNet::loadFrom(char * path)
{
	int size;
	FILE *f = fopen(path, "rt");
	fscanf(f, "%d", &size);
	for (int i = 0; i < size; i++)
	{
		LayerType type;
		fscanf(f, "%d", &type);
		switch (type)
		{
		case LinearType:
			layers.push_back(Linear::loadFrom(f));
			break;
		case ConvolutionalType:
			layers.push_back(SpacialConvolution::loadFrom(f));
			break;
		case MaxPoolingType:
			layers.push_back(SpacialMaxPooling::loadFrom(f));
			break;
		case DropoutType:
			layers.push_back(Dropout::loadFrom(f));
			break;

		case FastSigmoidType:
			layers.push_back(new FastSigmoid());
			break;
		case SoftmaxType:
			layers.push_back(new Softmax());
			break;
		case ReLUType:
			layers.push_back(new ReLU());
			break;
		case LogSoftmaxType:
			layers.push_back(new LogSoftmax());
			break;
		case SigmoidType:
			layers.push_back(new Sigmoid());
			break;
		case TanhType:
			layers.push_back(new Tanh());
			break;
		default:
			throw - 1;
		}
	}

	fclose(f);
}

void NeuralNet::printNetwork()
{
	for (int i = 0; i < layers.size(); i++)
	{
		printf("%s\n", layerTypeToString(layers[i]->type));
	}
}

void NeuralNet::setTrainMode(bool value)
{
	for (int i = 0; i < layers.size(); i++)
	{
		layers[i]->train_mode = value;
	}
}

void NeuralNet::setOptimizer(Optimizer optimizer)
{
	for (int i = 0; i < layers.size(); i++)
	{
		layers[i]->setOptimizer(optimizer);
	}
}

void NeuralNet::setNormalizeError(bool value)
{
	for (int i = 0; i < layers.size(); i++)
	{
		layers[i]->normalize_error = value;
	}
}