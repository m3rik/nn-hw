#include "Tanh.h"

#include <math.h>
#include <ppl.h>

Tanh::Tanh()
{
	type = TanhType;
}


Tanh::~Tanh()
{
}

Tensor Tanh::forward(Tensor input)
{
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), input.sz, [&](int i)
	{
#else
	for (int i = 0; i < input.sz; i++)
	{
#endif
		input[i] = tanh(input[i]);
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	return input;
}

void Tanh::derivative(Tensor output, Tensor doutputs)
{
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), doutputs.sz, [&](int i)
	{
#else
	for (int i = 0; i < doutputs.sz; i++)
	{
#endif
		doutputs[i] = 1 - output[i] * output[i];
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif
}