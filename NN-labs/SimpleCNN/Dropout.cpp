#include "Dropout.h"
#include <assert.h>
#include <ppl.h>

Dropout::Dropout(double probability)
{
	type = DropoutType;
	this->probability = probability;
}


Dropout::~Dropout()
{
	outputs.free();
	errors.free();
	doutputs.free();
}

Tensor Dropout::forward(Tensor input)
{
	if (input.sz != outputs.sz)
	{
		outputs = Tensor(input.d3, input.d2, input.d1);
		errors = Tensor(input.d3, input.d2, input.d1);
		doutputs = Tensor(input.d3, input.d2, input.d1);
	}

	if (train_mode)
	{

#if C_MODE == PARALLEL_FOR_WIN
		concurrency::parallel_for(int(0), input.sz, [&](int i)
		{
#else
		for (int i = 0; i < input.sz; i++)
		{
#endif
			int factor = ((double)rand()) / RAND_MAX > probability ? 1 : 0;
			outputs[i] = input[i] * factor;
		}
#if C_MODE == PARALLEL_FOR_WIN
		);
#endif
	}
	else
	{
		input.copyTo(outputs);
	}

	return outputs;
}


bool Dropout::backPropagation(Tensor target, Layer *next)
{
	assert(outputs.d1 == target.d1);
	assert(outputs.d2 == target.d2);
	assert(outputs.d3 == target.d3);

	if (next != NULL)
	{
		next->derivative(outputs, doutputs);
	}
	else
	{
		derivative(outputs, doutputs);
	}


	for (int i = 0; i < errors.sz; i++)
	{
		errors[i] = (target[i] - outputs[i]) * doutputs[i];
	}

	return true;
}

bool Dropout::backPropagation(Layer *last, Layer *next)
{
	next->derivative(outputs, doutputs);
	last->backPropagation(errors, doutputs);
	return true;
}

bool Dropout::backPropagation(Tensor errors, Tensor doutputs)
{
	for (int i = 0; i < errors.sz; i++)
	{
		if (outputs[i] == 0)
		{
			errors[i] = 0;
		}
		else
		{
			errors[i] = this->errors[i] * doutputs[i];
		}
	}
	return true;
}

Tensor Dropout::updateWeights(Tensor input)
{
	return outputs;
}

void Dropout::saveTo(FILE *f)
{
	fprintf(f, "%le\n", probability);
}
Layer* Dropout::loadFrom(FILE *f)
{
	double probability;
	fscanf(f, "%le", &probability);
	return new Dropout(probability);
}