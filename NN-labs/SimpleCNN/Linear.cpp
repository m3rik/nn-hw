#include "Linear.h"

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cassert>

#include <ppl.h>

#include <opencv2\opencv.hpp>
using namespace cv;

Linear::Linear(int input, int output)
{
	type = LinearType;
	weights = Tensor(input, output);
	updatesWeights = Tensor(input, output);
	deltaWeights= Tensor(input, output);
	momentumWeights = Tensor(input, output);

	outputs = Tensor(output);
	doutputs = Tensor(output);
	
	bias = Tensor(output);
	deltaBias= Tensor(output);
	updatesBias = Tensor(output);
	momentumBias = Tensor(output);

	errors = Tensor(output);

	double b = sqrt(1.0 / input);
	weights.gaussianInit(0, b);

	Mat weightsMat(weights.d2, weights.d3, CV_64FC1, &weights[0]);

	bias.constInit(0);

	deltaWeights.zeros();
	deltaBias.zeros();
	updatesWeights.zeros();
	updatesBias.zeros();
	momentumWeights.zeros();
	momentumBias.zeros();
}


Linear::~Linear()
{
	weights.free();
	updatesWeights.free();
	deltaWeights.free();
	outputs.free();
	doutputs.free();
	bias.free();
	deltaBias.free();
	updatesBias.free();
	errors.free();
	for (int i = 0; i < optimAux.size(); i++)
	{
		optimAux[i].free();
	}
}

#pragma optimize("",off)
 void Linear::quantization(int m, int k)
{
	 quantizedWeights = Tensor(weights.d3 / m, k * m);
	 quantizedBuffer = Tensor(weights.d3 / m, k);
	 quantizedIndexes = Tensor(weights.d3 / m, weights.d2);
	 quantizedK = k;
	 quantizedM = m;
	 
	 Mat weightsMat(weights.d2, weights.d3, CV_64FC1, &weights[0]);
	 weightsMat.convertTo(weightsMat, CV_32FC1);
	 for (int i = 0; i < weights.d3 / m; i++)
	 {
		 Mat qWeights = weightsMat(Rect(i * m, 0, m, weights.d2));
		 Mat indexes;
		 Mat centers;
		 kmeans(qWeights, k, indexes, cv::TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 100, 0.000001), 10, KMEANS_PP_CENTERS, centers);

		 centers.convertTo(centers, CV_64FC1);
		 indexes.convertTo(indexes, CV_64FC1);

		 memcpy(&quantizedWeights[quantizedWeights.d2 * i], centers.data, quantizedWeights.d2 * sizeof(double));
		 memcpy(&quantizedIndexes[quantizedIndexes.d2 * i], indexes.data, quantizedIndexes.d2 * sizeof(double));
	 }
	 isQuantized = true;
}
#pragma optimize("",on)


 Tensor Linear::forwardQuantized(Tensor input)
 {
	 outputs.zeros();
	 quantizedBuffer.zeros();

#if C_MODE == PARALLEL_FOR_WIN
	 concurrency::parallel_for(int(0), input.sz / quantizedM, [&](int m)
	 {
#else
	 for (int m = 0; m < input.sz / quantizedM; m++)
	 {
#endif
		 int qb = m * quantizedBuffer.d2;
		 int qi = m * quantizedIndexes.d2;
		 int qw = m * quantizedWeights.d2;

		 for (int j = 0; j < quantizedK; j++)
		 {
			 for (int i = 0; i < quantizedM; i++)
			 {
				 quantizedBuffer[qb + j] += input[m * quantizedM + i] * quantizedWeights[qw + j * quantizedM + i];
			 }
		 }
		 for (int i = 0; i < outputs.sz; i++)
		 {
			outputs[i] += quantizedBuffer[qb + (int)quantizedIndexes[qi + i]];
		
		 }
	 }
#if C_MODE == PARALLEL_FOR_WIN
	 );
#endif
	
	 for (int i = 0; i < outputs.sz; i++)
	{
		outputs[i] += bias[i];
	}

	 return outputs;
 }

Tensor Linear::forward(Tensor input)
{
	assert(input.sz == weights.d3);

	if (isQuantized)
		return forwardQuantized(input);
	
	outputs.zeros();

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), outputs.sz, [&](int i)
	{
#else
	for (int i = 0; i < outputs.sz; i++)
	{
#endif
		for (int j = 0; j < input.sz; j++)
		{
			outputs[i] += input[j] * weights[i * weights.d3 + j];
		}
		outputs[i] += bias[i];
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	return outputs;
}


void Linear::weightsInit(WeightInit *initializer)
{
	initializer->initWeights(weights);
	initializer->initWeights(bias);
}

bool Linear::backPropagation(Tensor target, Layer *next)
{
	if (next != NULL)
	{
		next->derivative(outputs, doutputs);
	}
	else
	{
		derivative(outputs, doutputs);
	}

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), errors.sz, [&](int i)
	{
#else
	for (int i = 0; i < errors.sz; i++)
	{
#endif		
		errors[i] = -(target[i] - outputs[i]) * doutputs[i];
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	return true;
}



bool Linear::backPropagation(Layer *last, Layer *next)
{
	next->derivative(outputs, doutputs);
	last->backPropagation(errors, doutputs);
	return true;
}

bool Linear::backPropagation(Tensor errors, Tensor doutputs)
{
	errors.zeros();
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), outputs.sz, [&](int j)
	{
#else
	for (int j = 0; j < outputs.sz; j++)
	{
#endif
		for (int i = 0; i < errors.sz; i++)
		{
			errors[i] += this->errors[j] * weights[j * weights.d3 + i];
		}
	
	}
#if C_MODE == PARALLEL_FOR_WIN
);
#endif

	for (int i = 0; i < errors.sz; i++)
	{
		errors[i] *= doutputs[i];
	}

	return true;
}

Tensor Linear::updateWeights(Tensor input)
{
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), outputs.d3, [&](int j)
	{
#else
	for (int j = 0; j < outputs.d3; j++)
	{
#endif
		for (int i = 0; i < input.sz; i++)
		{
			int cur_index = j * weights.d3 + i;

			double change = errors[j] * input[i];
			updatesWeights[cur_index] += change + weights[cur_index] * weightDecay;
		}

		double change = errors[j];
		updatesBias[j] += change;
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif
	

	return outputs;
}

void Linear::applyWeightsDefault()
{
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), weights.sz, [&](int i)
	{
#else
	for (int i = 0; i < weights.sz; i++)
	{
#endif	
		double change = deltaWeights[i] * momentum + learning_rate * updatesWeights[i];
		weights[i] -= change;
		deltaWeights[i] = change;
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	for (int j = 0; j < outputs.d3; j++)
	{
		double change = deltaBias[j] * momentum + learning_rate * updatesBias[j];
		bias[j] -= change;
		deltaBias[j] = change;
	}
}

void Linear::applyWeightsNesterov()
{
	if (optimAux.empty())
	{
		Tensor aWeights(weights.d3, weights.d2);
		Tensor aBias(outputs.d3);
	
		optimAux.push_back(aWeights);
		optimAux.push_back(aBias);
	}

	Tensor &aWeights = optimAux[0];
	Tensor &aBias = optimAux[1];

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), weights.sz, [&](int i)
	{
#else
	for (int i = 0; i < weights.sz; i++)
	{
#endif
		double nest_mom = momentum * deltaWeights[i] + learning_rate * updatesWeights[i];
		weights[i] = aWeights[i] - nest_mom;
		aWeights[i] = weights[i];
		weights[i] -= momentum * nest_mom;
		deltaWeights[i] = nest_mom;
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), outputs.sz, [&](int i)
	{
#else
	for (int i = 0; i < outputs.sz; i++)
	{
#endif
		double nest_mom = momentum * deltaBias[i] + learning_rate * updatesBias[i];
		bias[i] = aBias[i] - nest_mom;
		aBias[i] = bias[i];
		bias[i] -= momentum * nest_mom;
		deltaBias[i] = nest_mom;
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

}

void Linear::applyWeightsAdam()
{
	AdamData *adam = (AdamData*)optimizer.data;

	if (optimAux.empty())
	{
		Tensor aWeights(weights.d3, weights.d2);
		Tensor aBias(outputs.d3);
		Tensor betaTensor(2);
		betaTensor[0] = adam->beta1;
		betaTensor[1] = adam->beta2;

		optimAux.push_back(aWeights);
		optimAux.push_back(aBias);
		optimAux.push_back(betaTensor);

		deltaBias.zeros();
		deltaWeights.zeros();
	}

	Tensor &aWeights = optimAux[0];
	Tensor &aBias = optimAux[1];
	Tensor &betaTensor = optimAux[2];

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), weights.sz, [&](int i)
	{
#else
	for (int i = 0; i < weights.sz; i++)
	{
#endif
		double mt = adam->beta1 * deltaWeights[i] + (1 - adam->beta1) * updatesWeights[i];
		double vt = adam->beta2 * aWeights[i] + (1 - adam->beta2) * pow(updatesWeights[i], 2);

		double mta = mt / (1 - betaTensor[0]);
		double vta = vt / (1 - betaTensor[1]);

		double tetha = learning_rate * mta / (sqrt(vta) + adam->epsilon);
		weights[i] -= tetha;
		
		deltaWeights[i] = mt;
		aWeights[i] = vt;
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), outputs.d3, [&](int i)
	{
#else
	for (int i = 0; i < outputs.d3; i++)
	{
#endif
		double mt = adam->beta1 * deltaBias[i] + (1 - adam->beta1) * updatesBias[i];
		double vt = adam->beta2 * aBias[i] + (1 - adam->beta2) * pow(updatesBias[i], 2);

		double mta = mt / (1 - betaTensor[0]);
		double vta = vt / (1 - betaTensor[1]);

		double tetha = learning_rate * mta / (sqrt(vta) + adam->epsilon);
		bias[i] -= tetha;

		deltaBias[i] = mt;
		aBias[i] = vt;
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	betaTensor[0] *= adam->beta1;
	betaTensor[1] *= adam->beta2;
}

void Linear::applyWeightsAdadelta()
{
	AdadeltaData *adadelta = (AdadeltaData*)optimizer.data;

	if (optimAux.empty())
	{
		Tensor adadeltaWeights(weights.d3, weights.d2);
		Tensor adadeltaBias(outputs.d3);

		optimAux.push_back(adadeltaWeights);
		optimAux.push_back(adadeltaBias);

		deltaBias.zeros();
		deltaWeights.zeros();
	}

	Tensor &aWeights = optimAux[0];
	Tensor &aBias = optimAux[1];

	for (int i = 0; i < weights.sz; i++)
	{
		double change =  adadelta->average * deltaWeights[i] + (1 - adadelta->average) * pow(updatesWeights[i], 2);
		double tetha = sqrt(aWeights[i] + adadelta->epsilon) * updatesWeights[i] / sqrt(change + adadelta->epsilon);
		weights[i] -= tetha;
		aWeights[i] = adadelta->average * aWeights[i] + (1 - adadelta->average) * pow(tetha, 2);
		deltaWeights[i] = change;
	}

	for (int i = 0; i < outputs.d3; i++)
	{
		double change = adadelta->average * deltaBias[i] + (1 - adadelta->average) * pow(updatesBias[i], 2);
		double tetha = sqrt(aBias[i] + adadelta->epsilon) * updatesBias[i] / sqrt(change + adadelta->epsilon);
		bias[i] -= tetha;
		aBias[i] = adadelta->average * aBias[i] + (1 - adadelta->average) * pow(tetha, 2);
		deltaBias[i] = change;
	}
}

void Linear::applyWeightsAdagrad()
{
	AdagradData *adagrad = (AdagradData*)optimizer.data;

	for (int i = 0; i < weights.sz; i++)
	{
		double change = deltaWeights[i] + pow(updatesWeights[i], 2);
		weights[i] -= learning_rate * updatesWeights[i] / sqrt(change + adagrad->epsilon);
		deltaWeights[i] = change;
	}

	for (int i = 0; i < outputs.d3; i++)
	{
		double change = deltaBias[i] + pow(updatesBias[i], 2);
		bias[i] -= learning_rate * updatesBias[i] / sqrt(change + adagrad->epsilon);
		deltaBias[i] = change;
	}
}

void Linear::applyWeightsRmsprop()
{
	RmspropData *rmsprop = (RmspropData*)optimizer.data;

	for (int i = 0; i < weights.sz; i++)
	{
		double change = deltaWeights[i] * rmsprop->average + (1 - rmsprop->average) * pow(updatesWeights[i], 2);
		weights[i] -= learning_rate * updatesWeights[i] / sqrt(change + rmsprop->epsilon);
		deltaWeights[i] = change;
	}

	for (int i = 0; i < outputs.d3; i++)
	{
		double change = deltaBias[i] * rmsprop->average + (1 - rmsprop->average) * pow(updatesBias[i], 2);
		bias[i] -= learning_rate * updatesBias[i] / sqrt(change + rmsprop->epsilon);
		deltaBias[i] = change;
	}
}

void Linear::applyWeightsRprop()
{
	RpropData *rprop = (RpropData*)optimizer.data;
	if (optimAux.empty())
	{
		Tensor rpropWeights(weights.d3, weights.d2);
		rpropWeights.constInit(rprop->delta_zero);
		
		Tensor rpropBias(outputs.d3);
		rpropBias.constInit(rprop->delta_zero);

		optimAux.push_back(rpropWeights);
		optimAux.push_back(rpropBias);

		deltaBias.zeros();
		deltaWeights.zeros();
	}

	Tensor &rWeights = optimAux[0];
	Tensor &rBias = optimAux[1];
	/* https://github.com/encog/encog-dotnet-core/blob/master/encog-core-cs/Neural/Networks/Training/Propagation/Resilient/ResilientPropagation.cs */
	for (int i = 0; i < weights.sz; i++)
	{
		if (deltaWeights[i] * updatesWeights[i] > 0)
		{
			rWeights[i] = std::min<double>(rWeights[i] * rprop->n_plus, rprop->delta_max);
			double change = -sgn<double>(updatesWeights[i]) * rWeights[i];
			weights[i] += change;
			deltaWeights[i] = updatesWeights[i];
		}
		else //if (deltaWeights[i] * updatesWeights[i] < 0)
		{
			rWeights[i] = std::max<double>(rWeights[i] * rprop->n_minus, rprop->delta_min);
			double change = -sgn<double>(updatesWeights[i]) * rWeights[i];
			weights[i] += change;
			deltaWeights[i] = 0;// updatesWeights[i];
		}
		/*else
		{
			double change = -sgn<double>(updatesWeights[i]) * rWeights[i];
			weights[i] += change;
			deltaWeights[i] = updatesWeights[i];
		}*/
	}

	for (int i = 0; i < outputs.d3; i++)
	{
		if (deltaBias[i] * updatesBias[i] > 0)
		{
			rBias[i] = std::min<double>(rBias[i] * rprop->n_plus, rprop->delta_max);
			double change = -sgn<double>(updatesBias[i]) * rBias[i];
			bias[i] += change;
			deltaBias[i] = updatesBias[i];
		}
		else if (deltaBias[i] * updatesBias[i] < 0)
		{
			rBias[i] = std::max<double>(rBias[i] * rprop->n_minus, rprop->delta_min);
			deltaBias[i] = 0;
		}
		else
		{
			double change = -sgn<double>(updatesBias[i]) * rBias[i];
			bias[i] += change;
			deltaBias[i] = updatesBias[i];
		}
	}
}

void Linear::applyWeights()
{
	switch (optimizer.strategy)
	{
		case NESTEROV:
		{
			applyWeightsNesterov();
			break;
		}
		case ADAM:
		{
			applyWeightsAdam();
			break;
		}
		case ADADELTA:
		{
			applyWeightsAdadelta();
			break;
		}
		case ADAGRAD:
		{
			applyWeightsAdagrad();
			break;
		}
		case RMSPROP:
		{
			applyWeightsRmsprop();
			break;
		}
		case RPROP:
		{
			applyWeightsRprop();
			break;
		}
		case DEFAULT:
		{
			applyWeightsDefault();
			break;
		}
	}
	zeroUpdates();
}

void Linear::zeroUpdates()
{
	updatesWeights.zeros();
	updatesBias.zeros();
}


void Linear::saveTo(FILE *f)
{
	fprintf(f, "%d %d\n", weights.d3, weights.d2);
	weights.saveTo(f);
	bias.saveTo(f);
}

Layer* Linear::loadFrom(FILE *f)
{
	int input, output;
	fscanf(f, "%d %d", &input, &output);
	Linear *linear = new Linear(input, output);
	linear->weights.free();
	linear->bias.free();
	linear->weights = Tensor::loadFrom(f);
	linear->bias = Tensor::loadFrom(f);

	return linear;
}

void Linear::setOptimizer(Optimizer optimizer)
{
	this->optimizer = optimizer;
}