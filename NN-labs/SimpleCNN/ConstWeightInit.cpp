#include "ConstWeightInit.h"


ConstWeightInit::ConstWeightInit(double value)
{
	this->value = value;
}


ConstWeightInit::~ConstWeightInit()
{
}


void ConstWeightInit::initWeights(Tensor &tensor)
{
	tensor.constInit(value);
}