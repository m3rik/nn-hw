#ifndef _RANDOM_WEIGHT_INIT_H_
#define _RANDOM_WEIGHT_INIT_H_

#include "WeightInit.h"

class RandomWeightInit :
	public WeightInit
{
	double low, high;
public:
	RandomWeightInit(double low, double high);
	~RandomWeightInit();
	virtual void initWeights(Tensor &tensor);
};

#endif