#ifndef _SIMPLE_CNN_H_
#define _SIMPLE_CNN_H_

#include "Trainer.h"
#include "NeuralNet.h"
#include "Layer.h"

#include "FastSigmoid.h"
#include "Sigmoid.h"
#include "Tanh.h"
#include "Softmax.h"
#include "ReLU.h"
#include "LogSoftmax.h"

#include "Linear.h"
#include "SpacialConvolution.h"
#include "SpacialMaxPooling.h"
#include "Dropout.h"

#include "Tensor.h"


#include "RandomWeightInit.h"
#include "ConstWeightInit.h"
#include "WeightInit.h"

#endif