#include "SpacialConvolution.h"
#include "Linear.h"

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>

#include <ppl.h>


SpacialConvolution::SpacialConvolution(int input, int output, int field, int height, int width, int stride, int dilation)
{
	type = ConvolutionalType;

	this->input = input;
	this->output = output;
	this->field = field;
	this->height = height;
	this->width = width;
	this->stride = stride;
	this->dilation = dilation;
	this->filter_sz = input * field * field;
	this->output_field = dilation * (field - 1) + 1;

	weights = Tensor(field, field, input * output);
	deltaWeights = Tensor(field, field, input * output);
	updatesWeights = Tensor(field, field, input * output);

	bias = Tensor(output);
	deltaBias = Tensor(output);
	updatesBias = Tensor(output);

	outputs = Tensor((width - output_field) / stride + 1, (height - output_field) / stride + 1, output);
	doutputs = Tensor((width - output_field) / stride + 1, (height - output_field) / stride + 1, output);
	errors = Tensor((width - output_field) / stride + 1, (height - output_field) / stride + 1, output);

	double fan_in = input * field * field;
	double fan_out = output * field * field;
	double b = sqrt(2.0 / (fan_in + fan_out));
	weights.gaussianInit(0, b);
	bias.constInit(0);

	decay_rate = 0.9;

	deltaWeights.zeros();
	deltaBias.zeros();
	updatesWeights.zeros();
	updatesBias.zeros();
}


SpacialConvolution::~SpacialConvolution()
{
	weights.free();
	updatesWeights.free();
	deltaWeights.free();
	outputs.free();
	doutputs.free();
	bias.free();
	deltaBias.free();
	updatesBias.free();
	errors.free();
	for (int i = 0; i < optimAux.size(); i++)
	{
		optimAux[i].free();
	}
}


Tensor SpacialConvolution::forward(Tensor inputs)
{
	
	
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for (int(0), output, [&](int o_id)
	{
#else
	for (int o_id = 0; o_id < output; o_id++)
	{
#endif
		for (int oi = 0; oi < outputs.d2; oi++)
		{
			for (int oj = 0; oj < outputs.d3; oj++)
			{
				double sum = 0;
				int min_i = oi * stride;
				int max_i = min_i + output_field;
				
				int min_j = oj * stride;
				int max_j = min_j + output_field;

				for (int i_id = 0; i_id < input; i_id++)
				{
					for (int i = min_i, ii = 0; i < max_i; i+=dilation, ii++)
					{
						for (int j = min_j, jj = 0; j < max_j; j+=dilation, jj++)
						{
							sum += inputs[i_id * inputs.l_sz + i * inputs.d3 + j]
								* weights[o_id * filter_sz + i_id * weights.l_sz + ii * field + jj];
						}
					}
				}
				sum += bias[o_id];
				outputs[o_id * outputs.l_sz + oi * outputs.d3 + oj] = sum;
			}
		}
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	return outputs;
}

bool SpacialConvolution::backPropagation(Tensor target, Layer *next)
{
	if (next != NULL)
		next->derivative(outputs, doutputs);
	else
	{
		derivative(outputs, doutputs);
	}


	for (int i = 0; i < errors.sz; i++)
	{
		errors[i] = -(target[i] - outputs[i]) * doutputs[i];
	}

	if(normalize_error)
		for (int i = 0; i < errors.sz; i++)
		{
			errors[i] /= errors.sz;
		}

	return true;
}

bool SpacialConvolution::backPropagation(Layer *last, Layer *next)
{
	next->derivative(outputs, doutputs);
	last->backPropagation(errors, doutputs);
	return true;
}

bool SpacialConvolution::backPropagation(Tensor errors, Tensor doutputs)
{

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), errors.d1, [&](int i_id)
	{
#else
	for (int i_id = 0; i_id < errors.d1; i_id++)
	{
#endif
		for (int oi = 0; oi < errors.d2; oi++)
		{
			for (int oj = 0; oj < errors.d3; oj++)
			{
				double error = 0;
				int min_i = oi / this->stride;
				int max_i = min_i - this->output_field;
				if (min_i >= this->errors.d2)
					min_i = this->errors.d2 - 1;
				if (max_i < -1)
					max_i = -1;

				int min_j = oj / this->stride;
				int max_j = min_j - this->output_field;
				if (min_j >= this->errors.d3)
					min_j = this->errors.d3 - 1;
				if (max_j < -1)
					max_j = -1;

				for (int o_id = 0; o_id < this->output; o_id++)
				{
					for (int i = min_i, ii = 0; i > max_i; i-=dilation, ii++)
					{
						for (int j = min_j, jj = 0; j > max_j; j-=dilation, jj++)
						{
							error += this->errors[o_id * this->outputs.l_sz + i * this->outputs.d3 + j]
								* this->weights[o_id * this->filter_sz + i_id * this->weights.l_sz + ii * this->field + jj];
						}
					}
				}

				errors[i_id * errors.l_sz + oi * errors.d3 + oj] = error * doutputs[i_id * doutputs.l_sz + oi * doutputs.d3 + oj];
			}
		}
	}

#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	return true;
}

Tensor SpacialConvolution::updateWeights(Tensor inputs)
{
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), output, [&](int o_id)
	{
#else
	for (int o_id = 0; o_id < output; o_id++)
	{
#endif
		for (int i_id = 0; i_id < input; i_id++)
		{
			for (int i = 0; i < field; i++)
			{
				for (int j = 0; j < field; j++)
				{
					double change = 0;
					
					for (int oi = 0; oi < outputs.d2; oi++)
					{
						for (int oj = 0; oj < outputs.d3; oj++)
						{
							change += errors[o_id * errors.l_sz + oi * errors.d3 + oj] *
								inputs[i_id * inputs.l_sz + (oi + i) * inputs.d3 + (oj + j)];
						}
					}

					int cur_index = o_id * filter_sz + i_id * weights.l_sz + i * field + j;
					updatesWeights[cur_index] += change + weights[cur_index] * weightDecay;
				}
			}
		}
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), output, [&](int o_id)
	{
#else
	for (int o_id = 0; o_id < output; o_id++)
	{
#endif
			double change = 0;

			for (int oi = 0; oi < outputs.d2; oi++)
			{
				for (int oj = 0; oj < outputs.d3; oj++)
				{
					change += errors[o_id * outputs.l_sz + oi * outputs.d3 + oj];
				}
			}
			updatesBias[o_id] += change;
	}

#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	return outputs;
}


void SpacialConvolution::applyWeightsDefault()
{
	for (int i = 0; i < weights.sz; i++)
	{
		double change = deltaWeights[i] * momentum + learning_rate * updatesWeights[i];
		weights[i] -= change;
		deltaWeights[i] = change;
	}

	for (int j = 0; j < bias.sz; j++)
	{
		double change = deltaBias[j] * momentum + learning_rate * updatesBias[j];
		bias[j] -= change;
		deltaBias[j] = change;
	}
	}

void SpacialConvolution::applyWeightsNesterov()
{
	if (optimAux.empty())
	{
		Tensor aWeights(weights.sz);
		Tensor aBias(bias.sz);

		optimAux.push_back(aWeights);
		optimAux.push_back(aBias);
	}

	Tensor &aWeights = optimAux[0];
	Tensor &aBias = optimAux[1];

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), weights.sz, [&](int i)
	{
#else
	for (int i = 0; i < weights.sz; i++)
	{
#endif
		double nest_mom = momentum * deltaWeights[i] + learning_rate * updatesWeights[i];
		weights[i] = aWeights[i] - nest_mom;
		aWeights[i] = weights[i];
		weights[i] -= momentum * nest_mom;
		deltaWeights[i] = nest_mom;
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), bias.sz, [&](int i)
	{
#else
	for (int i = 0; i < bias.sz; i++)
	{
#endif
		double nest_mom = momentum * deltaBias[i] + learning_rate * updatesBias[i];
		bias[i] = aBias[i] - nest_mom;
		aBias[i] = bias[i];
		bias[i] -= momentum * nest_mom;
		deltaBias[i] = nest_mom;
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif


	}

void SpacialConvolution::applyWeightsAdam()
{
	AdamData *adam = (AdamData*)optimizer.data;

	if (optimAux.empty())
	{
		Tensor aWeights(weights.sz);
		Tensor aBias(bias.sz);
		Tensor betaTensor(2);
		betaTensor[0] = adam->beta1;
		betaTensor[1] = adam->beta2;

		optimAux.push_back(aWeights);
		optimAux.push_back(aBias);
		optimAux.push_back(betaTensor);

		deltaBias.zeros();
		deltaWeights.zeros();
	}

	Tensor &aWeights = optimAux[0];
	Tensor &aBias = optimAux[1];
	Tensor &betaTensor = optimAux[2];

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), weights.sz, [&](int i)
	{
#else
	for (int i = 0; i < weights.sz; i++)
	{
#endif
		double mt = adam->beta1 * deltaWeights[i] + (1 - adam->beta1) * updatesWeights[i];
		double vt = adam->beta2 * aWeights[i] + (1 - adam->beta2) * pow(updatesWeights[i], 2);

		double mta = mt / (1 - betaTensor[0]);
		double vta = vt / (1 - betaTensor[1]);

		double tetha = learning_rate * mta / (sqrt(vta) + adam->epsilon);
		weights[i] -= tetha;

		deltaWeights[i] = mt;
		aWeights[i] = vt;
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), bias.sz, [&](int i)
	{
#else
	for (int i = 0; i < bias.sz; i++)
	{
#endif
		double mt = adam->beta1 * deltaBias[i] + (1 - adam->beta1) * updatesBias[i];
		double vt = adam->beta2 * aBias[i] + (1 - adam->beta2) * pow(updatesBias[i], 2);

		double mta = mt / (1 - betaTensor[0]);
		double vta = vt / (1 - betaTensor[1]);

		double tetha = learning_rate * mta / (sqrt(vta) + adam->epsilon);
		bias[i] -= tetha;

		deltaBias[i] = mt;
		aBias[i] = vt;
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	betaTensor[0] *= adam->beta1;
	betaTensor[1] *= adam->beta2;
	}

void SpacialConvolution::applyWeightsAdadelta()
{
	AdadeltaData *adadelta = (AdadeltaData*)optimizer.data;

	if (optimAux.empty())
	{
		Tensor adadeltaWeights(weights.sz);
		Tensor adadeltaBias(bias.sz);

		optimAux.push_back(adadeltaWeights);
		optimAux.push_back(adadeltaBias);

		deltaBias.zeros();
		deltaWeights.zeros();
	}

	Tensor &aWeights = optimAux[0];
	Tensor &aBias = optimAux[1];

	for (int i = 0; i < weights.sz; i++)
	{
		double change = adadelta->average * deltaWeights[i] + (1 - adadelta->average) * pow(updatesWeights[i], 2);
		double tetha = sqrt(aWeights[i] + adadelta->epsilon) * updatesWeights[i] / sqrt(change + adadelta->epsilon);
		weights[i] -= tetha;
		aWeights[i] = adadelta->average * aWeights[i] + (1 - adadelta->average) * pow(tetha, 2);
		deltaWeights[i] = change;
	}

	for (int i = 0; i < bias.sz; i++)
	{
		double change = adadelta->average * deltaBias[i] + (1 - adadelta->average) * pow(updatesBias[i], 2);
		double tetha = sqrt(aBias[i] + adadelta->epsilon) * updatesBias[i] / sqrt(change + adadelta->epsilon);
		bias[i] -= tetha;
		aBias[i] = adadelta->average * aBias[i] + (1 - adadelta->average) * pow(tetha, 2);
		deltaBias[i] = change;
	}
}

void SpacialConvolution::applyWeightsAdagrad()
{
	AdagradData *adagrad = (AdagradData*)optimizer.data;

	for (int i = 0; i < weights.sz; i++)
	{
		double change = deltaWeights[i] + pow(updatesWeights[i], 2);
		weights[i] -= learning_rate * updatesWeights[i] / sqrt(change + adagrad->epsilon);
		deltaWeights[i] = change;
	}

	for (int i = 0; i < bias.sz; i++)
	{
		double change = deltaBias[i] + pow(updatesBias[i], 2);
		bias[i] -= learning_rate * updatesBias[i] / sqrt(change + adagrad->epsilon);
		deltaBias[i] = change;
	}
}

void SpacialConvolution::applyWeightsRmsprop()
{
	RmspropData *rmsprop = (RmspropData*)optimizer.data;

	for (int i = 0; i < weights.sz; i++)
	{
		double change = deltaWeights[i] * rmsprop->average + (1 - rmsprop->average) * pow(updatesWeights[i], 2);
		weights[i] -= learning_rate * updatesWeights[i] / sqrt(change + rmsprop->epsilon);
		deltaWeights[i] = change;
	}

	for (int i = 0; i < bias.sz; i++)
	{
		double change = deltaBias[i] * rmsprop->average + (1 - rmsprop->average) * pow(updatesBias[i], 2);
		bias[i] -= learning_rate * updatesBias[i] / sqrt(change + rmsprop->epsilon);
		deltaBias[i] = change;
	}
}

void SpacialConvolution::applyWeightsRprop()
{
	RpropData *rprop = (RpropData*)optimizer.data;
	if (optimAux.empty())
	{
		Tensor rpropWeights(weights.sz);
		rpropWeights.constInit(rprop->delta_zero);

		Tensor rpropBias(bias.sz);
		rpropBias.constInit(rprop->delta_zero);

		optimAux.push_back(rpropWeights);
		optimAux.push_back(rpropBias);

		deltaBias.zeros();
		deltaWeights.zeros();
	}

	Tensor &rWeights = optimAux[0];
	Tensor &rBias = optimAux[1];

	for (int i = 0; i < weights.sz; i++)
	{
		if (deltaWeights[i] * updatesWeights[i] > 0)
		{
			rWeights[i] = std::min<double>(rWeights[i] * rprop->n_plus, rprop->delta_max);
			double change = -sgn<double>(updatesWeights[i]) * rWeights[i];
			weights[i] += change;
			deltaWeights[i] = updatesWeights[i];
		}
		else if (deltaWeights[i] * updatesWeights[i] < 0)
		{
			rWeights[i] = std::max<double>(rWeights[i] * rprop->n_minus, rprop->delta_min);
			deltaWeights[i] = 0;
		}
		else
		{
			double change = -sgn<double>(updatesWeights[i]) * rWeights[i];
			weights[i] += change;
			deltaWeights[i] = updatesWeights[i];
		}
	}

	for (int i = 0; i < bias.sz; i++)
	{
		if (deltaBias[i] * updatesBias[i] > 0)
		{
			rBias[i] = std::min<double>(rBias[i] * rprop->n_plus, rprop->delta_max);
			double change = -sgn<double>(updatesBias[i]) * rBias[i];
			bias[i] += change;
			deltaBias[i] = updatesBias[i];
		}
		else if (deltaBias[i] * updatesBias[i] < 0)
		{
			rBias[i] = std::max<double>(rBias[i] * rprop->n_minus, rprop->delta_min);
			deltaBias[i] = 0;
		}
		else
		{
			double change = -sgn<double>(updatesBias[i]) * rBias[i];
			bias[i] += change;
			deltaBias[i] = updatesBias[i];
		}
	}
}

void SpacialConvolution::applyWeights()
{
	switch (optimizer.strategy)
	{
	case NESTEROV:
	{
		applyWeightsNesterov();
		break;
	}
	case ADAM:
	{
		applyWeightsAdam();
		break;
	}
	case ADADELTA:
	{
		applyWeightsAdadelta();
		break;
	}
	case ADAGRAD:
	{
		applyWeightsAdagrad();
		break;
	}
	case RMSPROP:
	{
		applyWeightsRmsprop();
		break;
	}
	case RPROP:
	{
		applyWeightsRprop();
		break;
	}
	case DEFAULT:
	{
		applyWeightsDefault();
		break;
	}
	}
	zeroUpdates();
}


void SpacialConvolution::zeroUpdates()
{
	updatesWeights.zeros();
	updatesBias.zeros();
}

void SpacialConvolution::weightsInit(WeightInit *initializer)
{
	initializer->initWeights(weights);
	initializer->initWeights(bias);
}

void SpacialConvolution::saveTo(FILE *f)
{
	fprintf(f, "%d %d %d %d %d %d\n", input, output, field, height, width, stride);
	weights.saveTo(f);
	bias.saveTo(f);
}

Layer* SpacialConvolution::loadFrom(FILE *f)
{
	int input, output, field, height, width, stride;
	fscanf(f, "%d %d %d %d %d %d", &input, &output, &field, &height, &width, &stride);
	SpacialConvolution *conv = new SpacialConvolution(input, output, field, height, width, stride);
	conv->weights.free();
	conv->bias.free();
	conv->weights = Tensor::loadFrom(f);
	conv->bias = Tensor::loadFrom(f);

	return conv;
}