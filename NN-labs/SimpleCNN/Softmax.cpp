#include "Softmax.h"

#include <math.h>
#include "MathUtils.h"

Softmax::Softmax()
{
	type = SoftmaxType;
}


Softmax::~Softmax()
{
}

Tensor Softmax::forward(Tensor input)
{
	double sum = 0;
	double max_val = input[input.indexOfMax()];
	for (int i = 0; i < input.sz; i++)
	{
		double val = ExpMinusApprox(-(input[i] - max_val));
		input[i] = val;
		sum += val;
	}

	for (int i = 0; i < input.sz; i++)
	{
		input[i] = input[i] / sum;
	}

	return input;
}

void Softmax::derivative(Tensor output, Tensor doutputs)
{
	double sum = 0;
	for (int i = 0; i < output.sz; i++)
	{
		sum += output[i];
	}

	for (int i = 0; i < doutputs.sz; i++)
	{
		doutputs[i] = output[i] * (sum - output[i]);
	}
}