#include "LogSoftmax.h"

#include <ppl.h>
#include <cmath>

LogSoftmax::LogSoftmax()
{
	type = LogSoftmaxType;
}


LogSoftmax::~LogSoftmax()
{
}

Tensor LogSoftmax::forward(Tensor input)
{
	double sum = 0;
	double max_val = input[input.indexOfMax()]; 

	for (int i = 0; i < input.sz; i++)
	{
		sum += exp(input[i] - max_val);
	}

	sum = log(sum) + max_val;

	for (int i = 0; i < input.sz; i++)
	{
		input[i] = input[i] / sum;
	}

	return input;
}

void LogSoftmax::derivative(Tensor output, Tensor doutputs)
{
	for (int i = 0; i < doutputs.sz; i++)
	{
		doutputs[i] = output[i] * (1 - output[i]);
	}
}