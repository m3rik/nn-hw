#include "RandomWeightInit.h"


RandomWeightInit::RandomWeightInit(double low, double high)
{
	this->low = low;
	this->high = high;
}


RandomWeightInit::~RandomWeightInit()
{
}


void RandomWeightInit::initWeights(Tensor &tensor)
{
	tensor.randInit(low, high);
}