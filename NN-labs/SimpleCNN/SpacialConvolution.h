#pragma once
#include "Layer.h"
class SpacialConvolution :
	public Layer
{
	int input, output, field, stride, dilation, output_field;
	int width, height, filter_sz;
	double decay_rate;

	Tensor outputs;
	Tensor doutputs;

	Tensor weights;
	Tensor deltaWeights;
	Tensor updatesWeights;

	Tensor bias;
	Tensor deltaBias;
	Tensor updatesBias;

	Tensor errors;

	std::vector<Tensor> optimAux;	

public:
	SpacialConvolution(int input, int output, int field, int height, int width, int stride = 1, int dilation = 1);
	~SpacialConvolution();

	virtual Tensor forward(Tensor input);
	virtual void weightsInit(WeightInit *initializer);

	virtual bool backPropagation(Tensor target, Layer *next);
	virtual bool backPropagation(Layer *last, Layer *next);
	virtual bool backPropagation(Tensor errors, Tensor doutputs);

	virtual Tensor updateWeights(Tensor input);
	virtual void applyWeights();

	void applyWeightsDefault();
	void applyWeightsRprop();
	void applyWeightsRmsprop();
	void applyWeightsAdagrad();
	void applyWeightsAdadelta();
	void applyWeightsAdam();
	void applyWeightsNesterov();

	virtual void zeroUpdates();

	virtual void saveTo(FILE *f);
	static Layer* loadFrom(FILE *f);
};

