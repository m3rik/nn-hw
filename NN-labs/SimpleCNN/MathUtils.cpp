#include "MathUtils.h"



double ExpMinusApprox(double x)
{

#define EXACT_EXPONENTIAL
#ifdef EXACT_EXPONENTIAL
	return exp(-x);
#else
	/* fast approximation of exp(-x) for x positive */
# define A0   (1.0)
# define A1   (0.125)
# define A2   (0.0078125)
# define A3   (0.00032552083)
# define A4   (1.0172526e-5)
	if (x < 13.0)
	{
		/*    assert(x>=0); */
		double y;
		y = A0 + x*(A1 + x*(A2 + x*(A3 + x*A4)));
		y *= y;
		y *= y;
		y *= y;
		y = 1 / y;
		return y;
	}
	return 0;
# undef A0
# undef A1
# undef A2
# undef A3
# undef A4
#endif
}
