#ifndef _RELU_H_
#define _RELU_H_

#include "Layer.h"

class ReLU :
	public Layer
{
	Tensor input;
public:
	ReLU();
	~ReLU();
	virtual Tensor forward(Tensor input);
	virtual void derivative(Tensor output, Tensor doutputs);
};

#endif