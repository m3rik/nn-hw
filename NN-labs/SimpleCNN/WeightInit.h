#ifndef _WEIGHT_INIT_H_
#define _WEIGHT_INIT_H_

#include "Tensor.h"

class WeightInit
{
public:
	WeightInit();
	~WeightInit();
	virtual void initWeights(Tensor& values);
};

#endif

