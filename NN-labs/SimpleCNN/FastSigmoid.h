#ifndef _FAST_SIGMOID_H_
#define _FAST_SIGMOID_H_

#include "Tensor.h"
#include "Layer.h"

class FastSigmoid : 
	public Layer
{
public:
	FastSigmoid();
	~FastSigmoid();
	virtual Tensor forward(Tensor input);
	virtual void derivative(Tensor output, Tensor doutputs);
};

#endif