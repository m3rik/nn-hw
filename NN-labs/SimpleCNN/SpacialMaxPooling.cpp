#include "SpacialMaxPooling.h"

#include <cstdlib>
#include <cstdio>
#include <cassert>

#include <ppl.h>

SpacialMaxPooling::SpacialMaxPooling(int input, int field, int height, int width)
{
	type = MaxPoolingType;

	this->input = input;
	this->field = field;
	this->height = height;
	this->width = width;

	outputs = Tensor(width / field, height / field, input);
	doutputs = Tensor(width / field, height / field, input);
	errors = Tensor(width / field, height / field, input);
	switches = Tensor(width / field, height / field, input);
	
}


SpacialMaxPooling::~SpacialMaxPooling()
{
	outputs.free();
	doutputs.free();
	errors.free();
	switches.free();
}


Tensor SpacialMaxPooling::forward(Tensor inputs)
{
	assert(inputs.d1 == this->input);
	assert(inputs.d2 == this->height);
	assert(inputs.d3 == this->width);

	outputs.constInit(-1000);

#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), input, [&](int i_id)
	{
#else
	for (int i_id = 0; i_id < input; i_id++)
	{
#endif
		int offset_input = inputs.l_sz * i_id;
		int offset_output = outputs.l_sz * i_id;
		for (int i = 0; i < inputs.d2 / field * field; i++)
		{
			for (int j = 0; j < inputs.d3 / field * field; j++)
			{
				double a = inputs[offset_input + i * inputs.d3 + j];
				double b = outputs[offset_output + i / field * outputs.d3 + j / field];
				outputs[offset_output + i / field * outputs.d3 + j / field] = a > b ? a : b;
				if (a > b)
				{
					switches[offset_output + i / field * outputs.d3 + j / field] = offset_input + i * inputs.d3 + j;
				}
			}
		}
	}

#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	return outputs;
}

bool SpacialMaxPooling::backPropagation(Tensor target, Layer *next)
{
	assert(outputs.d1 == target.d1);
	assert(outputs.d2 == target.d2);
	assert(outputs.d3 == target.d3);

	if (next != NULL)
	{
		next->derivative(outputs, doutputs);
	}
	else
	{
		derivative(outputs, doutputs);
	}


	for (int i = 0; i < errors.sz; i++)
	{
		errors[i] = (target[i] - outputs[i]) * doutputs[i];
	}

	return true;
}

bool SpacialMaxPooling::backPropagation(Layer *last, Layer *next)
{
	next->derivative(outputs, doutputs);
	last->backPropagation(errors, doutputs);
	return true;
}

bool SpacialMaxPooling::backPropagation(Tensor errors, Tensor doutputs)
{
	assert(errors.d1 == this->input);
	assert(errors.d2 == this->height);
	assert(errors.d3 == this->width);

	errors.zeros();
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), switches.sz, [&](int i)
	{
#else
	for (int i = 0; i < switches.sz; i++)
	{
#endif
		errors[(int)switches[i]] = this->errors[i] * doutputs[(int)switches[i]];
		
	}

#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	return true;
}

Tensor SpacialMaxPooling::updateWeights(Tensor input)
{
	return outputs;
}

void SpacialMaxPooling::saveTo(FILE *f)
{
	fprintf(f, "%d %d %d %d\n", input, field, height, width);
}

Layer* SpacialMaxPooling::loadFrom(FILE *f)
{
	int input, field, height, width;
	fscanf(f, "%d %d %d %d", &input, &field, &height, &width);
	return new SpacialMaxPooling(input, field, height, width);
}