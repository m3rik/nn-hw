#pragma once
#include "WeightInit.h"
class ConstWeightInit :
	public WeightInit
{
	double value;
public:
	ConstWeightInit(double value);
	~ConstWeightInit();
	virtual void initWeights(Tensor &tensor);
};

