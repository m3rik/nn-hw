#pragma once
#include <opencv2\core.hpp>
#include "Tensor.h"

using namespace cv;

Tensor mat2tensor(Mat mat)
{
	Tensor tensor(mat.cols, mat.rows, mat.channels());
	for (int y = 0; y < mat.rows; y++)
	{
		for (int x = 0; x < mat.cols; x++)
		{
			Vec3b pixel;
			if (mat.channels() == 3)
				pixel = mat.at<Vec3b>(Point(x, y));
			else
				pixel[0] = mat.at<uchar>(Point(x, y));

			for (int c = 0; c < mat.channels(); c++)
			{
				tensor[c * tensor.l_sz + y * tensor.d3 + x] = pixel[c] / 255.0;
			}
		}
	}
	return tensor;
}

Mat tensor2mat(Tensor tensor)
{
	Mat mat;
	if (tensor.d1 == 1)
		mat = Mat::zeros(Size(tensor.d3, tensor.d2), CV_8UC1);
	else
		mat = Mat::zeros(Size(tensor.d3, tensor.d2), CV_8UC3);

	for (int y = 0; y < tensor.d2; y++)
	{
		for (int x = 0; x < tensor.d3; x++)
		{
			Vec3b pixel;
			for (int c = 0; c < tensor.d1; c++)
			{
				char value = (char)(tensor[c * tensor.l_sz + y * tensor.d3 + x] * 255);
				pixel[c] = value;
			}
			if (tensor.d1 == 1)
				mat.at<uchar>(Point(x, y)) = pixel[0];
			else
				mat.at<Vec3b>(Point(x, y)) = pixel;
		}
	}
	return mat;
}
