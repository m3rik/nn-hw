#pragma once


enum OptimizerStrategy
{
	DEFAULT, RPROP, RMSPROP, ADAGRAD, ADADELTA, ADAM, NESTEROV
};

struct OptimizerData
{
	
};

struct RpropData : OptimizerData
{
	float n_plus = 1.2f;
	float n_minus = 0.5f;
	float delta_zero = 0.1f;
	float delta_max = 50.0f;
	float delta_min = 1e-6f;
	RpropData(float _n_minus = 0.5f, float _n_plus = 1.2f, float _delta_min = 1e-4, float _delta_max = 10.0f, 
		float _delta_zero = 0.1f) : n_minus(_n_minus), n_plus(_n_plus), delta_min(_delta_min), delta_max(_delta_max), 
		delta_zero(_delta_zero) {}
};

struct RmspropData : OptimizerData
{
	float average = 0.9f;
	float epsilon = 1e-6f;	
	RmspropData(float _average = 0.9f) : average(_average) {}
};

struct AdagradData : OptimizerData
{
	float epsilon = 1e-8f;
	AdagradData(float _epsilon = 1e-8f) : epsilon(_epsilon) {}
};

struct AdadeltaData : OptimizerData
{
	float average = 0.9f;
	float epsilon = 1e-6f;
	AdadeltaData(float _average = 0.9f) : average(_average) {}
};

struct AdamData : OptimizerData
{
	float beta1 = 0.9f;
	float beta2 = 1e-6f;
	float epsilon = 1e-8f;
	AdamData(float _beta1 = 0.9f, float _beta2 = 0.999f) : beta1(_beta1), beta2(_beta2) {}
};


struct NesterovData : OptimizerData
{
	NesterovData() {}
};


struct Optimizer
{
	OptimizerStrategy strategy;
	OptimizerData *data;

	Optimizer() {}
	Optimizer(OptimizerStrategy _strategy, OptimizerData *_data) : strategy(_strategy), data(_data) {}

};
