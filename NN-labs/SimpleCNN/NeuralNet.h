#include <vector>

#include "Layer.h"
#include "Tensor.h"
#include "WeightInit.h"
#include "Optimizer.h"

#ifndef _NEURAL_NET_H_
#define _NEURAL_NET_H_

using namespace std;

class NeuralNet
{
private:
	vector<Layer*> layers;
public:
	NeuralNet();
	~NeuralNet();
	void addLayer(Layer *layer);
	Tensor forward(Tensor input);
	void backPropagation(Tensor target);
	void updateWeights(Tensor input);
	void applyUpdates();

	vector<Layer*>* getLayers();
	void weightsInit(WeightInit *initializer);
	void zeroUpdates();
	void setTrainMode(bool value);
	void setOptimizer(Optimizer optimizer);
	void setNormalizeError(bool value);

	void saveTo(char *path);
	void loadFrom(char * path);
	void printNetwork();
};

#endif