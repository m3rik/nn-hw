#pragma once

#include <opencv2\opencv.hpp>

using namespace cv;
using namespace std;


namespace BoundingBoxHelper
{

	Point point1, point2; /* vertical points of the bounding box */
	int drag;
	Rect rect; /* bounding box */
	Mat img, roiImg; /* roiImg - the part of the image in the bounding box */
	int select_flag;

	void mouseHandler(int event, int x, int y, int flags, void* param)
	{
		if (event == CV_EVENT_LBUTTONDOWN && !drag)
		{
			/* left button clicked. ROI selection begins */
			point1 = Point(x, y);
			drag = 1;
		}

		if (event == CV_EVENT_MOUSEMOVE && drag)
		{
			/* mouse dragged. ROI being selected */
			Mat img1 = img.clone();
			point2 = Point(x, y);
			rectangle(img1, point1, point2, CV_RGB(255, 0, 0), 3, 8, 0);
			imshow("image", img1);
		}

		if (event == CV_EVENT_LBUTTONUP && drag)
		{
			point2 = Point(x, y);
			rect = Rect(point1.x, point1.y, x - point1.x, y - point1.y);
			drag = 0;

			int w = rect.width;
			int h = rect.height;
			int x = rect.x - w;
			int y = rect.y - h;
			w *= 3;
			h *= 3;
			Rect rect2(x, y, w, h);
			Mat drawImg = img.clone();
			rectangle(drawImg, rect, Scalar(255, 0, 0), 5);
			roiImg = drawImg(rect2);
		}

		if (event == CV_EVENT_LBUTTONUP)
		{
			/* ROI selected */
			select_flag = 1;
			drag = 0;
		}
	}


	Rect getROI(Mat origimg)
	{
		drag = 0;
		select_flag = 0;
		img = origimg;
		int k;
		namedWindow("image", CV_WINDOW_NORMAL | CV_WINDOW_KEEPRATIO);
		namedWindow("ROI", CV_WINDOW_NORMAL | CV_WINDOW_KEEPRATIO);
		resizeWindow("image", 800, 800);
		resizeWindow("ROI", 300, 300);
		imshow("image", img);
		while (1)
		{
			cvSetMouseCallback("image", mouseHandler, NULL);
			if (BoundingBoxHelper::select_flag == 1)
			{
				imshow("ROI", roiImg); /* show the image bounded by the box */
			}
			//Mat newImg = img.clone();
			//rectangle(newImg, rect, CV_RGB(255, 0, 0), 3, 8, 0);
			//imshow("image", newImg);
			k = waitKey(10);
			if (k == 27)
			{
				break;
			}
			
		}
		return rect;
	}
};

Mat getRotatedTemplate(Mat src, double angle, int interpolation = INTER_NEAREST, bool auto_roi = false)
{
	// get rotation matrix for rotating the image around its center
	cv::Point2f center(src.cols / 2.0, src.rows / 2.0);
	cv::Mat rot = cv::getRotationMatrix2D(center, angle, 1.0);
	// determine bounding rectangle
	cv::Rect bbox = cv::RotatedRect(center, src.size(), angle).boundingRect();
	// adjust transformation matrix
	rot.at<double>(0, 2) += bbox.width / 2.0 - center.x;
	rot.at<double>(1, 2) += bbox.height / 2.0 - center.y;

	cv::Mat dst;
	cv::warpAffine(src, dst, rot, bbox.size(), interpolation);

	if (auto_roi)
	{
		Rect roi = boundingRect(dst);
		dst = dst(roi);
	}

	return dst;
}