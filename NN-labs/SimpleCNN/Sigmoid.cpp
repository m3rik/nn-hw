#include "Sigmoid.h"

#include <math.h>

#include <cstdio>
#include <ppl.h>

Sigmoid::Sigmoid()
{
	type = SigmoidType;
}


Sigmoid::~Sigmoid()
{

}


Tensor Sigmoid::forward(Tensor input)
{
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), input.sz, [&](int i)
	{
#else
	for (int i = 0; i < input.sz; i++)
	{
#endif
		input[i] = 1.0 / (1.0 + exp(-input[i]));
	}

#if C_MODE == PARALLEL_FOR_WIN
	);
#endif
	
	return input;
}

void Sigmoid::derivative(Tensor output, Tensor doutputs)
{
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), doutputs.sz, [&](int i)
	{
#else
	for (int i = 0; i < doutputs.sz; i++)
	{
#endif
		doutputs[i] = output[i] * (1 - output[i]);
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

}

