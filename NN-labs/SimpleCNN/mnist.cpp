#include "mnist.h"

static void changeEndianess(int *value)
{
	char *v = (char*)value;
	char aux;
	aux = v[0];
	v[0] = v[3];
	v[3] = aux;
	aux = v[1];
	v[1] = v[2];
	v[2] = aux;
}

void read_image_file(char *path, double ***_inputs, int *len, int max)
{
	int magic_number, height, width, i, j;
	int ret;
	unsigned char value;
	double **inputs;

	FILE *f;
	fopen_s(&f, path, "rb");
	assert(f != NULL);

	ret = fread(&magic_number, sizeof(int), 1, f);
	changeEndianess(&magic_number);
	assert(ret == 1);
	assert(magic_number == 2051);

	ret = fread(len, sizeof(int), 1, f);
	assert(ret == 1);
	changeEndianess(len);

	*len = *len > max ? max : *len;

	inputs = (double**)malloc(*len * sizeof(double*));

	ret = fread(&height, sizeof(int), 1, f);
	assert(ret == 1);
	ret = fread(&width, sizeof(int), 1, f);
	assert(ret == 1);
	changeEndianess(&height);
	changeEndianess(&width);

	for (i = 0; i < *len; i++) {
		inputs[i] = (double* )malloc(sizeof(double)* width * height);
		for (j = 0; j < width * height; j++) {
			ret = fread(&value, 1, 1, f);
			assert(ret == 1);
			inputs[i][j] = (double)value / 255;
		}
	}

	*_inputs = inputs;

	fclose(f);
}

void read_label_file(char *path, double ***_outputs, int *len, int max)
{
	int magic_number, i;
	int ret;
	unsigned char value;
	double **outputs;

	FILE *f;
	fopen_s(&f, path, "rb");
	assert(f != NULL);

	ret = fread(&magic_number, sizeof(int), 1, f);
	assert(ret == 1);
	changeEndianess(&magic_number);
	assert(magic_number == 2049);

	ret = fread(len, sizeof(int), 1, f);
	assert(ret == 1);
	changeEndianess(len);

	*len = *len > max ? max : *len;

	outputs = (double**)malloc(*len * sizeof(double*));

	for (i = 0; i < *len; i++) {
		outputs[i] = (double*)calloc(sizeof(double), 10);
		ret = fread(&value, 1, 1, f);
		assert(ret == 1);
		outputs[i][value] = 1;
	}

	*_outputs = outputs;

	fclose(f);
}