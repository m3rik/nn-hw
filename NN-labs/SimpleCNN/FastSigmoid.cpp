#include "FastSigmoid.h"

#include <cmath>

FastSigmoid::FastSigmoid()
{
	type = FastSigmoidType;
}


FastSigmoid::~FastSigmoid()
{
}


Tensor FastSigmoid::forward(Tensor input)
{
	for (int i = 0; i < input.sz; i++)
	{
		input[i] = 1.0 / (1.0 + abs(input[i]));
	}
	return input;
}

void FastSigmoid::derivative(Tensor output, Tensor doutputs)
{
	for (int i = 0; i < doutputs.sz; i++)
	{
		doutputs[i] = -output[i] * output[i];
	}
}