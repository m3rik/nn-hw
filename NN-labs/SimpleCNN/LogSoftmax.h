#pragma once
#include "Layer.h"
class LogSoftmax :
	public Layer
{
public:
	LogSoftmax();
	~LogSoftmax();
	virtual Tensor forward(Tensor input);
	virtual void derivative(Tensor output, Tensor doutputs);
};

