#ifndef _TRAINER_H_
#define _TRAINER_H_

#include "NeuralNet.h"
#include "Optimizer.h"

class Trainer
{
	NeuralNet *net;
	double momentum;
	int batch_size;
	int iterations;
	Optimizer optimizer;

public:
	double learning_rate;
	Trainer(NeuralNet *net, Optimizer optimizer, double learning_rate = 0.1, double momentum = 0.9, int batch_size = 1, int iterations = 10);
	~Trainer();

	void train(vector<Tensor> &inputs, vector<Tensor> &outputs, bool debug = false, bool normalize_error = false);
	double test(vector<Tensor> &inputs, vector<Tensor> &targets, bool(*f)(Tensor& output, Tensor& target));
	double test(vector<Tensor> &inputs, vector<Tensor> &targets, double(*f)(Tensor& output, Tensor& target));
	
private:
	void trainOne(vector<Tensor> &inputs, vector<Tensor> &outputs);
	void randomize(vector<Tensor> &inputs, vector<Tensor> &outputs);

};

#endif

