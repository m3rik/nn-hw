#include "ReLU.h"

#include <ppl.h>

ReLU::ReLU()
{
	type = ReLUType;
}


ReLU::~ReLU()
{
}

Tensor ReLU::forward(Tensor input)
{
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), input.sz, [&](int i)
	{
#else
	for (int i = 0; i < input.sz; i++)
	{
#endif
		input[i] = input[i] > 0 ? input[i] : 0;
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

	this->input = input;
	return input;
}

void ReLU::derivative(Tensor output, Tensor doutputs)
{
#if C_MODE == PARALLEL_FOR_WIN
	concurrency::parallel_for(int(0), input.sz, [&](int i)
	{
#else
	for (int i = 0; i < input.sz; i++)
	{
#endif

		if (input[i] > 0)
		{
			doutputs[i] = 1;
		}
		else
		{
			doutputs[i] = 0;
		}
	}
#if C_MODE == PARALLEL_FOR_WIN
	);
#endif

}