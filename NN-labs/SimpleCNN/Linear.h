#ifndef _LINEAR_H_
#define _LINEAR_H_

#include "Layer.h"
#include "Tensor.h"

class Linear :
	public Layer
{
public:
	double decay_rate;

	Tensor weights;
	Tensor deltaWeights;
	Tensor updatesWeights;
	Tensor momentumWeights;
	
	Tensor outputs;
	Tensor doutputs;
	
	Tensor bias;
	Tensor deltaBias;
	Tensor updatesBias;
	Tensor momentumBias;
	
	Tensor errors;

	Tensor quantizedWeights;
	Tensor quantizedIndexes;
	Tensor quantizedBuffer;
	int quantizedM;
	int quantizedK;
	bool isQuantized;

	std::vector<Tensor> optimAux;

	Linear(int input, int output);
	~Linear();

	virtual void quantization(int m, int k);

	virtual Tensor forward(Tensor input);
	virtual Tensor forwardQuantized(Tensor input);
	virtual void weightsInit(WeightInit *initializer);

	virtual bool backPropagation(Tensor target, Layer *next); 
	virtual bool backPropagation(Layer *last, Layer *next);
	virtual bool backPropagation(Tensor errors, Tensor doutputs);

	virtual Tensor updateWeights(Tensor input);
	virtual void applyWeights();
	
	void applyWeightsDefault();
	void applyWeightsRprop();
	void applyWeightsRmsprop();
	void applyWeightsAdagrad();
	void applyWeightsAdadelta();
	void applyWeightsAdam();
	void applyWeightsNesterov();

	virtual void zeroUpdates();


	virtual void saveTo(FILE *f);
	static Layer* loadFrom(FILE *f);

	virtual void setOptimizer(Optimizer optimizer);

};

#endif