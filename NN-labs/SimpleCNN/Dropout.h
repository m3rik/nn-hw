#pragma once
#include "Layer.h"
class Dropout :
	public Layer
{
	double probability;
	Tensor outputs;
	Tensor doutputs;
	Tensor errors;

public:
	Dropout(double probability);
	~Dropout();
	virtual Tensor forward(Tensor input);

	virtual bool backPropagation(Tensor target, Layer *next);
	virtual bool backPropagation(Layer *last, Layer *next);
	virtual bool backPropagation(Tensor errors, Tensor doutputs);

	virtual Tensor updateWeights(Tensor input);

	virtual void saveTo(FILE *f);
	static Layer* loadFrom(FILE *f);

};

