# Simple CNN #

## Simple implementation of Convolutional Neural Networks ##

* ** Layers **
* ** Activation Functions **
* ** Learning **

### Layers ###

* SpatialConvolution
* SpacialMaxPooling
* Linear


### Activation Functions ###

* Sigmoid
* Tanh
* Softmax

### Learning ###

* Backpropagation
* Stochastic Gradient Descent