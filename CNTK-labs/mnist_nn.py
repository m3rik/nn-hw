import matplotlib.pyplot as plt

import numpy as np
import sys
import os
import pickle

from sklearn.preprocessing import LabelEncoder

from cntk import Trainer, cntk_device, StreamConfiguration, learning_rate_schedule, momentum_schedule, UnitType
from cntk.utils import get_train_eval_criterion, get_train_loss
from cntk.device import cpu, set_default_device
from cntk.learner import sgd, adagrad, adam_sgd, rmsprop, nesterov, momentum_sgd
from cntk.ops import cross_entropy_with_softmax, sigmoid, relu, tanh, input_variable, softmax, classification_error
from cntk.layers import Dense, Convolution, MaxPooling
from cntk.models import Sequential, LayerStack
from cntk.initializer import glorot_uniform, glorot_normal
from dataset_utils import DatasetManager

def preprocessInputs(data):
    return np.array(data, dtype=np.float32).reshape((data.shape[0], 1, 28, 28)) / 255

def preprocessOutputs(Y):
    Y = Y.reshape((Y.shape[0], 1))
    class_ind = [Y == class_number for class_number in range(10)]
    Y = np.asarray(np.hstack(class_ind), dtype=np.float32)
    return Y


def prepareData(train, test, limit=None):
    if limit == None:
        limit = train.shape[0]
    train_in = train[:limit, :-1]
    train_out = train[:limit, -1]
    test_in = test[:, :-1]
    test_out = test[:, -1]
    train_in = preprocessInputs(train_in)
    test_in = preprocessInputs(test_in)
    train_out = preprocessOutputs(train_out)
    test_out = preprocessOutputs(test_out)

    return train_in, train_out, test_in, test_out

def buildLeNet(input, out_dims):
    net = Convolution((5, 5), 8, init=glorot_normal(), activation=relu)(input)
    net = MaxPooling((2, 2), strides=(2, 2))(net)

    net = Convolution((5, 5), 16, init=glorot_normal(), activation=relu)(net)
    net = MaxPooling((2, 2), strides=(2, 2))(net)

    net = Dense(64, init=glorot_normal(),activation=relu)(net)
    net = Dense(out_dims, init=glorot_normal(), activation=None)(net)
    return net

def buildTwoLayers(input, out_dims):
    net = Dense(128, init=glorot_normal(),activation=relu)(input)
    net = Dense(out_dims, init=glorot_normal(), activation=None)(net)
    return net

if __name__ == '__main__':
    with open('data/MNIST/train.pickle', 'rb') as handle:
        train = pickle.load(handle)
    with open('data/MNIST/test.pickle', 'rb') as handle:
        test = pickle.load(handle)

    train_in, train_out, test_in, test_out = prepareData(train, test)

    in_h = 28
    in_w = 28
    out_dims = 10

    input_var = input_variable((1, in_h, in_w))
    label_var = input_variable((out_dims))

    net = buildLeNet(input_var, out_dims)

    ce = cross_entropy_with_softmax(net, label_var)
    pe = classification_error(net, label_var)

    epochs = 30

    lr_val = 0.1
    mom_val = 0.9
    batch_size = 32
    lr = learning_rate_schedule(lr_val, UnitType.minibatch)
    mo = momentum_schedule(mom_val)

    f = open('../Results/CNTK/cnn_rprop_hybrid.csv', 'wt')

    f.write('lr: ' + str(lr_val) + ' mom: ' + str(mom_val) + ' batch_size: ' + str(batch_size) + '\n')
    f.flush()

    #learner = sgd(net.parameters, lr=lr, gradient_clipping_with_truncation=False)
    #learner = momentum_sgd(net.parameters, lr=lr, momentum=mo)
    #learner = adam_sgd(net.parameters, lr=lr, momentum=mo)
    learner = rmsprop(net.parameters, lr, 0.99, 1.2, 0.5, 10, 0.1)
    #learner = adagrad(net.parameters, lr)
    #learner = nesterov(net.parameters, lr, mo)


    datasetManager = DatasetManager(train_in, train_out, test_in, test_out, batch_size)
    trainer = Trainer(net, ce, pe, [learner])

    for epoch in range(epochs):
        while True:
            inputs, labels = datasetManager.query_batch()
            if inputs is None:
                break
            data = {
                input_var : inputs,
                label_var : labels
            }
            trainer.train_minibatch(data)

        inputs, labels = datasetManager.query_train_batch()
        data = {
            input_var: inputs,
            label_var: labels
        }
        train_error = trainer.test_minibatch(data)

        inputs, labels = datasetManager.query_test_batch()
        data = {
            input_var: inputs,
            label_var: labels
        }
        test_error = trainer.test_minibatch(data)
        print("Iteration " + str(epoch + 1))
        print("Test error: " + str(train_error))
        print("Test error: " + str(test_error))

        f.write(str(epoch) + ',' + str(train_error) + ',' + str(test_error) + '\n')
        f.flush()

    f.close()



