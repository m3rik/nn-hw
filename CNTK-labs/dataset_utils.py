class DatasetManager:
    def __init__(self, train_in, train_out, test_in, test_out, batch_size):
        self.ds = [train_in, train_out, test_in, test_out]
        self.batch_size = batch_size
        self.index = 0

    def query_batch(self):
        [train_in, train_out, test_in, test_out] = self.ds
        if self.index + self.batch_size >= train_in.shape[0]:
            self.index = 0
            return None, None

        tin = train_in[self.index : self.index + self.batch_size, :]
        tout = train_out[self.index: self.index + self.batch_size]
        self.index += self.batch_size
        return tin, tout

    def query_train_batch(self):
        [train_in, train_out, test_in, test_out] = self.ds
        return train_in, train_out

    def query_test_batch(self):
        [train_in, train_out, test_in, test_out] = self.ds
        return test_in, test_out

