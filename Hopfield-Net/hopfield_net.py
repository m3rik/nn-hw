import numpy as np
from skimage import  io, transform

def readDigits():
    digits = np.zeros((10, 120, 1), np.float32)
    f = open('digits', 'rt')
    lines = f.readlines()
    f.close()
    y = 0
    for i in range(0, len(lines), 11):
        digit = lines[i:i + 10]
        digit = map(lambda x: x[:-1], digit)
        x = 0
        for d in digit:
            for c in d:
                digits[y][x][0] = -1
                if c == 'x':
                    digits[y][x][0] = 1
                x += 1
        y += 1
    return digits

def printDigit(digit):
    print
    for i in range(10):
        subset = digit[i * 12 : (i * 12 + 12)]
        for i in range(12):
            print 'x' if subset[i] == 1 else ' ',
        print
    print

def computeWeights(digits, M):
    weights = np.zeros((120, 120), np.float32)

    for i in range(M):
        weights += np.dot(digits[i], np.transpose(digits[i]))
    weights -= M * np.eye(120)
    return weights

def classify(weights, pattern):
    for i in range(1000):
        old_pattern = np.array(pattern)
        product = np.dot(weights, pattern)

        pattern = np.sign(product)
        if np.array_equal(old_pattern, pattern):
            break
    return pattern

def getPic(digit):
    return (np.array(digit).reshape((10,12)) + 1) / 2

def getFaulty(digit, F):
    digit = np.array(digit)
    import random
    visited = []
    while F > 0:
        index = random.randint(0, 10 * 12 - 1)
        if index in visited:
            continue
        visited.append(index)
        digit[index] *= (-1)
        F -= 1
    return digit

if __name__ == '__main__':
    digits = readDigits()

    #printDigit(digits[0])
    for M in range(1, 11):
        for F in range(0, 61):
            weights = computeWeights(digits, M)

            np_image = None

            for i in range(10):
                faultyDigit = getFaulty(digits[i], F)
                result = classify(weights, faultyDigit)

                faultyDigit = getPic(faultyDigit)
                result = getPic(result)

                pair = np.concatenate((faultyDigit, np.ones((3,12), np.float32) / 2, result))
                if np_image is None:
                    np_image = pair
                else:
                    np_image = np.concatenate((np_image, np.ones((23, 5), np.float32) / 2, pair), 1)

            np_image = transform.rescale(np_image, 6, 0)

            filepath = 'outputs/' + str(M) + 'patterns_' + str(F) + 'noise.png'
            io.imsave(filepath, np_image)